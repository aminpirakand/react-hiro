import React from "react";
import "./Refrence.css";
const Refrence = () => {
    return (
        <div>


            <form name="theForm" id="theForm">
                <div className="refrence-main-parent">
                    <p className="black form-text h2 mt" align="center">Reference Station</p>
                    <div className="row">
                        <div className="col-6">

                            <div id="contentDiv">
                                <table className="refrence-table" >
                                    <tbody>
                                        <tr>
                                            <td >
                                                <p className="black">CMR ID:</p>
                                            </td>
                                            <td ><input type="text" name="CMRID" id="CMRID" value="0" maxLength="2" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td >RTCM 2.x ID:</td>
                                            <td ><input type="text" name="RTCMID" id="RTCMID" value="0" maxLength="4" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td>RTCM 3.x ID:</td>
                                            <td><input type="text" name="RTCM3ID" id="RTCM3ID" value="0" maxLength="4" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td>Station Name:</td>
                                            <td><input type="text" name="StationName" id="StationName" value="CREF0001" maxlength="16" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td>Station Code:</td>
                                            <td><input type="text" name="StationCode" id="StationCode" value="" maxlength="16" disabled="disabled" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="checkradio">
                                    <input type="radio" name="coordSystem" value="Cartesian" onClick="coordSystemChange()" disabled="disabled" className="black" /><p className="black inline-block">&nbsp;Cartesian</p>&nbsp;<input type="radio" name="coordSystem" value="Geographical" checked="on" onClick="coordSystemChange()" disabled="disabled" /><p className="black inline-block">&nbsp;Geographical</p>
                                </div>
                                <br />
                                <br />
                                <table className="refrence-table" >
                                    <tbody>
                                        <tr id="refLatRow">
                                            <td >Reference Latitude:</td>
                                            <td><input type="text" name="refLatDeg" id="refLatDeg" value="" style={{width:"65px"}} maxlength="8" />°
                                                <input type="text" name="refLatMin" id="refLatMin" value="" style={{width:"50px"}} maxlength="2" />'
                                                <input type="text" name="refLatSec" id="refLatSec" value="" style={{width:"150px"}} maxlength="2" />"
                                                <input type="radio" name="refLatSign" value="1" />N<input type="radio" name="refLatSign" value="-1" />S</td>
                                        </tr>
                                        <tr id="refLonRow">
                                            <td >Reference Longitude:</td>
                                            <td><input type="text" name="refLonDeg" id="refLonDeg"  value="" style={{width:"65px"}} maxlength="8" />°
                                    <input type="text" name="refLonMin" id="refLonMin" value="" style={{width:"50px"}} maxlength="2" />'
                                    <input type="text" name="refLonSec" id="refLonSec" value="" style={{width:"150px"}} maxlength="2" />"
                                    <input type="radio" name="refLonSign" value="1" />E
                                    <input type="radio" name="refLonSign" value="-1" />W</td>
                                        </tr>
                                        <tr id="refHtRow">
                                            <td >Reference Height:</td>
                                            <td><input type="text" name="refHgt" id="refHgt" value="" maxlength="8" /> [m]</td>
                                        </tr>
                                        <tr id="refXRow" style={{ display: "none" }}>
                                            <td >Reference X:</td>
                                            <td><input type="text" name="refX" id="refX" value="" maxlength="13" /> [m]</td>
                                        </tr>
                                        <tr id="refYRow" style={{ display: "none" }}>
                                            <td >Reference Y:</td>
                                            <td><input type="text" name="refY" id="refY" value="" maxlength="13" /> [m]</td>
                                        </tr>
                                        <tr id="refZRow" style={{ display: "none" }}>
                                            <td >Reference Z:</td>
                                            <td><input type="text" name="refZ" id="refZ" value="" maxlength="13" /> [m]</td>
                                        </tr>
                                        <tr id="">
                                            <td ></td>
                                            <td><input className="mb" type="button" name="Here" id="Here" value="Here" onClick="hereAction()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Load Current Position</td>
                                        </tr>
                                        <tr id="">
                                            <td></td>
                                            <td><input className="mb" type="button" name="AvgPosButton" id="AvgPosButton" value="Average" onClick="loadAvgPos()" />&nbsp;&nbsp;Load Average Position</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <input type="hidden" name="herePositionUsed" id="herePositionUsed" value="0" disabled="disabled" />
                                <input type="hidden" name="hereLat" id="hereLat" value="" disabled="disabled" />
                                <input type="hidden" name="hereLon" id="hereLon" value="" disabled="disabled" />
                                <input type="hidden" name="hereHgt" id="hereHgt" value="" disabled="disabled" />
                                <input type="hidden" name="refLat" id="refLat" value="" disabled="disabled" />
                                <input type="hidden" name="refLon" id="refLon" value="" disabled="disabled" />
                                <input type="hidden" name="hereX" id="hereX" value="" disabled="disabled" />
                                <input type="hidden" name="hereY" id="hereY" value="" disabled="disabled" />
                                <input type="hidden" name="hereZ" id="hereZ" value="" disabled="disabled" />
                                <div id="posAvgContent"> <br />
                                    <table className="refrence-table" >
                                        <tbody>
                                            <tr>
                                                <td height="10px"></td>
                                            </tr>
                                            <tr>
                                                <td><b>Position Averaging</b></td>
                                            </tr>
                                            <tr>
                                                <td><b>Current Position:</b></td>
                                            </tr>
                                            <tr >
                                                <td>Lat</td>
                                                <td>39°</td>
                                                <td>53'</td>
                                                <td>49.55712"</td>
                                                <td>N</td>
                                            </tr>
                                            <tr >
                                                <td>Lon</td>
                                                <td>105°</td>
                                                <td> 6'</td>
                                                <td>56.34341"</td>
                                                <td>W</td>
                                            </tr>
                                            <tr >
                                                <td>Hgt</td>
                                                <td>1671.412</td>
                                                <td> [m]</td>
                                            </tr>
                                            <tr>
                                                <td><b>Average Position:</b></td>
                                            </tr>
                                            <tr >
                                                <td>Time</td>
                                                <td>3d</td>
                                                <td>5h</td>
                                                <td>38m</td>
                                                <td>54s</td>
                                            </tr>
                                            <tr>
                                                <td>Lat</td>
                                                <td>39°</td>
                                                <td>53'</td>
                                                <td>49.55691"</td>
                                                <td>N</td>
                                            </tr>
                                            <tr>
                                                <td>Lon</td>
                                                <td>105°</td>
                                                <td> 6'</td>
                                                <td>56.35399"</td>
                                                <td>W</td>
                                            </tr>
                                            <tr>
                                                <td>Hgt</td>
                                                <td>1671.696</td>
                                                <td> [m]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br />
                                <div id="posAvgCtrls">
                                    <table className="refrence-table ">
                                        <tbody>
                                            <tr>
                                                <td id="ResetAvgRow"><input type="button" className="mb" name="Reset" id="Reset" value="Reset Average" onClick="resetAvg()" disabled="disabled" /></td>
                                            </tr>
                                            <tr>
                                                <td id="AutoAvgChkBox">Auto Average: <input type="checkbox" name="AutoAvg" onClick="autoAverageClick()" /></td>
                                                <td id="AutoAvgTextBox" style={{ display: "none" }}>Time<input type="text" name="AutoAvgTime" id="AutoAvgTime" value="120" maxlength="3" disabled="disabled" />Sec. Range: 20 - 600 Seconds</td>
                                            </tr>
                                            <tr>
                                                <td id="CancelAutoAvgRow" style={{ display: "none" }}><input type="button" name="CancelAutoAvg" id="CancelAutoAvg" value="Cancel Auto Average" onClick="cancelAutoAverage()" disabled="disabled" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br />
                                <div id="submitButtons">
                                    <input type="button" className="mb" name="OK" id="OK" value="OK" onClick="okayAction()" disabled="disabled" />&nbsp;&nbsp;
                            <input type="button" className="mb" name="cancel" id="cancel" value="Cancel" onClick="loadDataFrameAfterDelay('xml/configDisplay.html',0)" />
                                </div>
                                <br />
                                <br />
                                <br />
                            </div>

                        </div>
                        <div className="col-6"></div>
                    </div>
                </div>
            </form>

        </div>
    )

};
export default Refrence