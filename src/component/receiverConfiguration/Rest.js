import React from "react"
import "./Rest.css"
const Rest = () => {
    return (
        <div>
            <form name="theForm" class="margin">
                <div className="rest-main-parent">
                    <p className="white h2">Receiver Reset</p>
                    <table className="rest-table">
                        <tbody>
                            <tr>
                                <td>Reboot Receiver:</td>
                                <td><input type="button" value="OK" onClick="receiverResetAction()" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>Use Default Application File:</td>
                                <td><input type="button" value="OK" onClick="useDefaultAppfileAction()" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>Clear Satellite Data:</td>
                                <td><input type="button" value="OK" onClick="clearSatelliteDataAction()" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>Clear Application Files:</td>
                                <td><input type="button" value="OK" onClick="clearAppFilesAction()" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>Clear All Receiver Settings:</td>
                                <td><input type="button" value="OK" onClick="clearAllReceiverSettingsAction()" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="button" name="cancel" id="cancel" value="Cancel" onclick="cancelAction()" /></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </form>
        </div>
    )
}
export default Rest