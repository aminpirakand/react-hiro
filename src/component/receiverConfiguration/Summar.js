import React from "react";
import "./Summar.css"
const Summar =()=>{
    return(
        <div >
            <div className="summar-main-parent col-xl-12">
                <p className="black form-text h2 mt-1 mb-5">Receiver Configuration</p>
                <table className="summar-table">
                    <tbody >
                    <tr>
                        <td ><b>Elevation Mask:</b></td>
                        <td >10°</td>
                    </tr>
                    <tr>
                        <td ><b>PDOP Mask:</b></td>
                        <td >99</td>
                    </tr>
                    <tr>
                        <td><b>Horizontal Precision:</b></td>
                        <td>0.30 [m]</td>
                    </tr>
                    <tr>
                        <td><b>Vertical Precision:</b></td>
                        <td>0.30 [m]</td>
                    </tr>
                    <tr>
                        <td><b>Clock Steering:</b></td>
                        <td>Disabled</td>
                    </tr>
                    <tr>
                        <td><b>Everest™ Multipath Mitigation:</b></td>
                        <td>Enabled</td>
                    </tr>
                    <tr>
                        <td><b>Signal Tracking Bandwidth:</b></td>
                        <td>Wide</td>
                    </tr>
                    <tr>
                        <td><b>Antenna ID:</b></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><b>Antenna Type:</b></td>
                        <td>Unknown External</td>
                    </tr>
                    <tr>
                        <td><b>Antenna Measurement Method:</b></td>
                        <td>Antenna Phase Center</td>
                    </tr>
                    <tr>
                        <td><b>Antenna Height:</b></td>
                        <td>0.0000 [m]</td>
                    </tr>
                    <tr>
                        <td><b>1PPS On/Off:</b></td>
                        <td>Disabled</td>
                    </tr>
                    <tr>
                        <td><b>Event 1 On/Off:</b></td>
                        <td>Disabled</td>
                    </tr>
                    <tr>
                        <td><b>Event 1 Slope:</b></td>
                        <td>Positive</td>
                    </tr>
                    <tr>
                        <td><b>RTK Mode:</b></td>
                        <td>Low Latency</td>
                    </tr>
                    <tr>
                        <td><b>Motion:</b></td>
                        <td>Kinematic</td>
                    </tr>
                    <tr>
                        <td><b>CMR Input Filter:</b></td>
                        <td>Disabled</td>
                    </tr>
                    <tr>
                        <td><b>Reference Latitude:</b></td>
                        <td>39°53'49.55373"N</td>
                    </tr>
                    <tr>
                        <td><b>Reference Longitude:</b></td>
                        <td>105°06'56.35467"W</td>
                    </tr>
                    <tr>
                        <td><b>Reference Height:</b></td>
                        <td>1672.014 [m]</td>
                    </tr>
                    <tr>
                        <td><b>RTCM 2.x ID:</b></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><b>RTCM 3.x ID:</b></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><b>CMR ID:</b></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><b>Station Name:</b></td>
                        <td>CREF0001</td>
                    </tr>
                    <tr>
                        <td><b>Ethernet IP:</b></td>
                        <td>155.63.160.21</td>
                    </tr>
                    <tr>
                        <td><b>System Name:</b></td>
                        <td>BD970 Test Drive</td>
                    </tr>
                    <tr>
                        <td><b>DNS Resolved Name:</b></td>
                        <td>----</td>
                    </tr>
                    <tr>
                        <td><b>Serial Number:</b></td>
                        <td>1051401040</td>
                    </tr>
                    <tr>
                        <td><b>Firmware Version:</b></td>
                        <td>5.42</td>
                    </tr>
                    <tr>
                        <td><b>Firmware Date:</b></td>
                        <td>2019-06-28</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )};
export default Summar
