import React from "react"
import "./ReciverTracking.css"
const ReceiverTracking = () => {
    return (
        <div>
            <form name="tracking">
                <div className="recivertraking-main-parent">

                    <p className="white form-text h2" align="center">Tracking</p>
                    <table className="recivertraking-table">
                        <tbody>
                            <tr>
                                <td>Elevation Mask</td>
                                <td><input type="text" name="elevMask" id="elevMask" value="10" size="3" maxlength="3" disabled="disabled" />°</td>
                            </tr>
                            <tr>
                                <td>Everest™</td>
                                <td><select name="Everest" id="Everest" disabled="disabled">
                                    <option value="Enabled" selected="on">Enable</option>
                                    <option value="Disabled">Disable</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Clock Steering</td>
                                <td><select name="ClockSteering" id="ClockSteering" disabled="disabled">
                                    <option value="Enabled">Enable</option>
                                    <option value="Disabled" selected="on">Disable</option>
                                </select></td>
                            </tr>
                        </tbody>
                    </table><br />
                    <table className="recivertraking-table" cellspacing="0" cellpadding="3">
                        <tbody>
                            <tr >
                                <th className="white">Type</th>
                                <th className="white">Signal</th>
                                <th className="white">Enable</th>
                                <th className="white">Options</th>
                            </tr>
                            <tr className="odd">
                                <td >GPS</td>
                                <td>L1 - C/A</td>
                                <td><input type="checkbox" name="L1CA_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>GPS</td>
                                <td>L2E</td>
                                <td><input type="checkbox" name="L2E_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="L2E_fallback" id="L2E_fallback" disabled="disabled">
                                    <option value="1" selected="on">L2C or L2E</option>
                                    <option value="0">L2C and L2E</option>
                                </select></td>
                            </tr>
                            <tr >
                                <td>GPS</td>
                                <td>L2C</td>
                                <td><input type="checkbox" name="L2C_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="L2C_mode" id="L2C_mode" disabled="disabled">
                                    <option value="CL">CL</option>
                                    <option value="CM_CL" selected="on">CM + CL</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>GPS</td>
                                <td>L5</td>
                                <td><input type="checkbox" name="L5_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="L5_mode" id="L5_mode" disabled="disabled">
                                    <option value="Q">Q</option>
                                    <option value="I_Q" selected="on">I + Q</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>SBAS</td>
                                <td>L1 - C/A</td>
                                <td><input type="checkbox" name="W1_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>SBAS</td>
                                <td>L5</td>
                                <td><input type="checkbox" name="W5_enable" onClick="checkTrack()" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>GLONASS</td>
                                <td>L1 - C/A</td>
                                <td><input type="checkbox" name="G1C_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>GLONASS</td>
                                <td>L1P</td>
                                <td><input type="checkbox" name="G1P_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>GLONASS</td>
                                <td>L2 - C/A</td>
                                <td><input type="checkbox" name="G2C_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="G2C_fallback" id="G2C_fallback" disabled="disabled">
                                    <option value="0" selected="on">L2 - C/A(M) Only</option>
                                    <option value="1">L2 - C/A(M) and P</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>GLONASS</td>
                                <td>L3</td>
                                <td><input type="checkbox" name="G3_enable" onClick="checkTrack()" disabled="disabled" /></td>
                                <td><select name="G3_mode" id="G3_mode" disabled="disabled" style={{ display: "none" }}>
                                    <option value="P">Pilot</option>
                                    <option value="D_P" selected="on">Data + Pilot</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Galileo</td>
                                <td>E1</td>
                                <td><input type="checkbox" name="E1_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="E1_mode" id="E1_mode" disabled="disabled" style={{ display: "none" }}>
                                    <option value="D">Data</option>
                                    <option value="P">Pilot</option>
                                    <option value="D_P" selected="on">Data + Pilot</option>
                                </select><input type="checkbox" name="E1_mboc" style={{ display: "none" }} checked="on" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>Galileo</td>
                                <td>E5 - A</td>
                                <td><input type="checkbox" name="E5A_enable" onClick="checkGalE5Track('E5A')" checked="on" disabled="disabled" /></td>
                                <td><select name="E5A_mode" id="E5A_mode" disabled="disabled" style={{ display: "none" }}>
                                    <option value="D">Data</option>
                                    <option value="P">Pilot</option>
                                    <option value="D_P" selected="on">Data + Pilot</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Galileo</td>
                                <td>E5 - B</td>
                                <td><input type="checkbox" name="E5B_enable" onClick="checkGalE5Track('E5B')" checked="on" disabled="disabled" /></td>
                                <td><select name="E5B_mode" id="E5B_mode" disabled="disabled" style={{ display: "none" }} >
                                    <option value="D">Data</option>
                                    <option value="P">Pilot</option>
                                    <option value="D_P" selected="on">Data + Pilot</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Galileo</td>
                                <td>E5 - AltBOC</td>
                                <td><input type="checkbox" name="E5AltBOC_enable" onClick="checkGalE5Track('E5AltBOC')" checked="on" disabled="disabled" /></td>
                                <td><select name="E5AltBOC_mode" id="E5AltBOC_mode" disabled="disabled" style={{ display: "none" }}>
                                    <option value="D">Data</option>
                                    <option value="P">Pilot</option>
                                    <option value="D_P" selected="on">Data + Pilot</option>
                                    <option value="FullAltBOC">Full AltBOC</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>BeiDou</td>
                                <td>B1</td>
                                <td><input type="checkbox" name="B1_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>BeiDou</td>
                                <td>B2</td>
                                <td><input type="checkbox" name="B2_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>QZSS</td>
                                <td>L1 - C/A</td>
                                <td><input type="checkbox" name="QZSSL1CA_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>QZSS</td>
                                <td>L1S</td>
                                <td><input type="checkbox" name="QZSSL1SLAS_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>QZSS</td>
                                <td>L2C</td>
                                <td><input type="checkbox" name="QZSSL2C_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="QZSSL2C_mode" id="QZSSL2C_mode" disabled="disabled" style={{ display: "none" }}>
                                    <option value="CL">CL</option>
                                    <option value="CM_CL" selected="on">CM + CL</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>QZSS</td>
                                <td>L5</td>
                                <td><input type="checkbox" name="QZSSL5_enable" onClick="checkTrack()" checked="on" disabled="disabled" /></td>
                                <td><select name="QZSSL5_mode" id="QZSSL5_mode" disabled="disabled" style={{ display: "none" }}>
                                    <option value="Q">Q</option>
                                    <option value="I_Q" selected="on">I + Q</option>
                                </select></td>
                            </tr>
                        </tbody>
                    </table><br />
                    <input type="button" class="mb" value="OK" onClick="Okay()" disabled="disabled" /><input type="button" name="cancel" id="cancel" value="Cancel" onclick="loadDataFrameAfterDelay('xml/configDisplay.html',0)" />

                </div>
            </form>
        </div>
    )
}
export default ReceiverTracking