import React from "react";
import "./Antenna.css";
import defult from "../../assets/img/default_gp.jpg"
const Antenna = () => {
    return (
        <div >

            <form name="theForm">
                <div className="antenna-main-parent">
                    <p align="center" className="h2 ">Antenna</p>
                    <table className="antenna-table">
                        <tbody>
                            <tr>
                                <td>Antenna Type</td>
                                <td>
                                    <select name="antennaID" id="antennaID" onChange="updatePage(-1)" disabled="disabled">
                                        <option value="572">33-3870 GNSS</option>
                                        <option value="387">111661</option>
                                        <option value="397">701941.B w/SCIS Dome</option>
                                        <option value="266">AG25 GNSS</option>
                                        <option value="521">AT1675-540TS</option>
                                        <option value="575">AV14</option>
                                        <option value="622">AV28</option>
                                        <option value="623">AV28 w/Ground Plane</option>
                                        <option value="403">AV34</option>
                                        <option value="404">AV37</option>
                                        <option value="478">AV39</option>
                                        <option value="309">AV59</option>
                                        <option value="192">EPOCH L1/L2</option>
                                        <option value="194">GA510</option>
                                        <option value="250">GA530</option>
                                        <option value="317">GA810</option>
                                        <option value="470">GA830</option>
                                        <option value="209">GNSS Choke Ring</option>
                                        <option value="148">GNSS-Ti Choke Ring</option>
                                        <option value="406">LV59</option>
                                        <option value="388">ProMark 500 Galileo</option>
                                        <option value="380">Rugged GA530</option>
                                        <option value="267">Tornado</option>
                                        <option value="523">UX5 HP Internal GNSS</option>
                                        <option value="0" selected="on">Unknown External</option>
                                        <option value="193">Z Plus</option>
                                        <option value="85">Zephyr</option>
                                        <option value="184">Zephyr - Model 2</option>
                                        <option value="410">Zephyr - Model 2 RoHS</option>
                                        <option value="249">Zephyr - Model 2 Rugged</option>
                                        <option value="570">Zephyr 3 Base</option>
                                        <option value="513">Zephyr 3 Geodetic</option>
                                        <option value="512">Zephyr 3 Rover</option>
                                        <option value="567">Zephyr 3 Rugged</option>
                                        <option value="86">Zephyr Geodetic</option>
                                        <option value="185">Zephyr Geodetic 2</option>
                                        <option value="265">Zephyr Geodetic 2 RoHS</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">RINEX Name</td>
                                <td>
                                    <select name="rinexName" id="rinexName" onChange="rinexChanged()" disabled="disabled">
                                        <option value="387">ASH111661&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="397">ASH701941.B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCIS</option>
                                        <option value="388">ASH802129&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="192">SPP39105.90&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="512">TRM105000.10&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>                                                    <option value="513">TRM115000.00&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="570">TRM115000.10&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="567">TRM126000_00&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="85">TRM39105.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="86">TRM41249.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="250">TRM44530.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="380">TRM44530R.00&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="470">TRM44830.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="194">TRM55550.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="184">TRM55970.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="185">TRM55971.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="193">TRM57200.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="410">TRM57970.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="265">TRM57971.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="267">TRM57972.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="209">TRM59800.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="148">TRM59900.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="249">TRM65212.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="266">TRM68040.01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="317">TRM99810.00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="521">TRMAT1675_540TS&nbsp;NONE</option>
                                        <option value="575">TRMAV14&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="622">TRMAV28&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="623">TRMAV28+GP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="404">TRMAV37&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="478">TRMAV39&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="309">TRMAV59&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="406">TRMLV59&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="403">TRM_AV34&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="572">TWI3870+GP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="523">UX5_HP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NONE</option>
                                        <option value="0" selected="on">Unknown&nbsp;External</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Antenna Serial Number</td>
                                <td><input type="text" name="antennaSerial" id="antennaSerial" value="" size="35" maxlength="31" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td align="left">Radome Serial Number</td>
                                <td><input type="text" name="radomeSerial" id="radomeSerial" value="" size="35" maxlength="31" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td align="left">Antenna Measurement Method</td>
                                <td>
                                    <select name="antennaMeasMethod" id="antennaMeasMethod" disabled="disabled">
                                        <option value="0">Bottom of antenna mount</option>
                                        <option value="255">Antenna Phase Center</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Antenna Height [m]</td>
                                <td><input type="text" name="antennaHeight" id="antennaHeight" value="0.0000" size="8" maxlength="6" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><img id="antennaImg" src={defult} alt="antenna" /></td>
                            </tr>
                            <tr>
                                <td align="left">Apply Antenna Correction to:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left">RTCM V3</td>
                                <td align="left"><input className="inp" type="checkbox" name="antennaCorrRtcmV3" checked="on" disabled="disabled" /></td>
                            </tr>

                        </tbody>
                    </table>
                    <div className="antenna-btn">
                        <input type="button" className="mb" name="OKButton" id="OKButton" value="OK" onClick="Okay() ;" disabled="disabled" />
                        <input type="button" className="" name="cancel" id="cancel" value="Cancel" onClick="loadDataFrameAfterDelay('xml/configDisplay.html',0)" />
                    </div>
                    <br/>
                    <br/>
                    <br/>
                </div>
            </form>


        </div>
    )
};
export default Antenna