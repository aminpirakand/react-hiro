import React from "react";
import add from "../../assets/img/add.gif"
import "./Correction.css"

const Correction = () => {
    return (
        <div className="correction-main-parent ">
            <p className="h2 mt-1 mb-5">Correction Controls</p>
            <div className="row">
                <div className="col-6">
                    <div>
                        <table className="correction-table">
                            <tbody>
                                <tr>
                                    <td><b>Input Filters:</b></td>
                                </tr>
                                <tr>
                                    <td>CMR Input Filter:<input type="checkbox" name="cmrFilter"
                                        onClick="showHideStationIds()"
                                        disabled="disabled" /></td>
                                    <td id="cmrId" style={{ display: "none" }}>&nbsp;&nbsp;ID:<input
                                        type="text" name="cmrFilterId" id="cmrFilterId" value="0" size="4"
                                        maxLength="2"
                                        disabled="disabled" />[0-31]</td>
                                </tr>
                                <tr>
                                    <td >RTCM Input Filter:<input type="checkbox" name="rtcmFilter"
                                        onClick="showHideStationIds()"
                                        disabled="disabled" /></td>
                                    <td id="rtcmId" style={{ display: "none" }}>&nbsp;&nbsp;ID:<input type="text" name="rtcmFilterId"
                                        id="rtcmFilterId" value="0" size="4"
                                        maxLength="4"
                                        disabled="disabled" />[0-4095]</td>
                                </tr>
                                <tr>
                                    <td>RTCM V2.4 Input:<select name="rtcmV2dot4Flag" id="rtcmV2dot4Flag"
                                        disabled="disabled">
                                        <option value="0" selected>Enable</option>
                                        <option value="1" >Disable</option>
                                    </select></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="rulesDiv">
                        <table className="correction-table" >
                            <tbody>
                                <tr>
                                    <th>RTK</th>
                                </tr>
                                <tr>
                                    <td>
                                        <table className="correction-table">
                                            <tbody>
                                                <tr>

                                                    <td ><a href="javascript:addRule('0','0') ;"
                                                        title="AddChannel"><img src={add}
                                                            align="center"
                                                            border="0" /></a></td>
                                                    <td></td>
                                                    <td><select name="defaultRule:0:0" id="defaultRule:0:0"
                                                        onChange="changeDefaultRule(0,0)" disabled="disabled">
                                                        <option value="AcceptAny" selected="on">Any Channel</option>
                                                        <option value="RejectAll">Reject All Channels</option>
                                                    </select></td>
                                                    <td ></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th>DGNSS</th>
                                </tr>
                                <tr>
                                    <td>
                                        <table className="correction-table"  >
                                            <tbody>
                                                <tr>
                                                    <td><a href=" javascript:addRule('0','0') ;"
                                                        title="AddChannel"><img align="center"
                                                            src={add}
                                                            border="0" /></a></td>
                                                    <td></td>
                                                    <td><select name="defaultRule:1:0" id="defaultRule:1:0"
                                                        onChange="changeDefaultRule(1,0)" disabled="disabled">
                                                        <option value="AcceptAny" selected="on">Any Channel</option>
                                                        <option value="RejectAll">Reject All Channels</option>
                                                    </select></td>
                                                    <td ></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="rtxCtrlsDiv">
                        <table width="35%" className="correction-table">
                            <tbody>
                                <tr>
                                    <td><b>RTX Controls:</b></td>
                                </tr>
                                <tr>
                                    <td>Disable RTX:<input type="checkbox" name="blockRTXCtrl"
                                        disabled="disabled" /></td>
                                </tr>
                                <tr>
                                    <td>Disable GVBS:<input type="checkbox" name="blockGvbsCtrl"
                                        disabled="disabled" /></td>
                                </tr>
                            </tbody>
                        </table>

                        <div className="corection-btn">
                            <input type="button" className="mb" name="okay" id="okay" value="OK" onClick="okayAction()" disabled="disabled" />
                            <input type="button" className="mb" name="cancel" id="cancel" value="Cancel" onClick="cancelAction()" />
                        </div>
                    </div>
                </div>
                <div className="col-6"></div>
            </div>

        </div>
    )
};
export default Correction