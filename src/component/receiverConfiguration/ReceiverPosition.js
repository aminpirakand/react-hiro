import React from "react"
import "./ReceiverPosition.css"
const ReceiverPosition = () => {
    return (
        <div>
            <form name="theForm">
                <div className="receiverposition-main-parent">

                    <p className="h2 white form-text">Position</p>
                    <table className="receiverposition-table"><input type="hidden" name="PVTType" disabled="disabled" />
                        <tbody>
                            <tr>
                                <td>PDOP Mask</td>
                                <td><input type="text" name="pdopMask" id="pdopMask" value="99" size="2" maxlength="2" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>RTK Mode</td>
                                <td><select name="RTKMode" id="RTKMode" disabled="disabled">
                                    <option value="0">Synchronous</option>
                                    <option value="1" selected="on">Low Latency</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>RTCM 2 Type 31 Input GLONASS Datum</td>
                                <td><select name="GlonassCorrectionsDatumMenu" id="GlonassCorrectionsDatumMenu" disabled="disabled">
                                    <option value="0" selected="on">PZ90</option>
                                    <option value="1">PZ90.02</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Autonomous/Differential Engine</td>
                                <td><select name="stingerPVT" id="stingerPVT" onchange="ShowHideSBASPlus() ;" disabled="disabled">
                                    <option value="0">Least Squares</option>
                                    <option value="1" selected="on">Kalman</option>
                                </select><select name="SBASPlus" id="SBASPlus" disabled="disabled">
                                        <option value="0" selected="on">SBAS</option>
                                        <option value="1">SBAS+</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td >Signal Tracking Bandwidth</td>
                                <td><select name="NarrowbandPLL" id="NarrowbandPLL" disabled="disabled">
                                    <option value="Enabled">Narrow</option>
                                    <option value="Disabled" selected="on">Wide</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Receiver Motion(Dynamic model)</td>
                                <td><select name="DynamicModel" id="DynamicModel" onchange="OnDynamicModelChange() ;" disabled="disabled">
                                    <option value="33">Static</option>
                                    <option value="0" selected="on">Kinematic</option>
                                    <option value="1">Human portable</option>
                                    <option value="2">Mapping vehicle</option>
                                    <option value="3">Off-road vehicle</option>
                                    <option value="4">Heavy equipment</option>
                                    <option value="5">Farm equipment</option>
                                    <option value="6">Airborne rotor</option>
                                    <option value="7">Airborne fixed wing</option>
                                    <option value="8">Marine</option>
                                    <option value="9">Rail</option>
                                    <option value="11">Automotive</option>
                                    <option value="12">Survey Pole</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>RTK Propagation Limit</td>
                                <td><select name="propTime" id="propTime" disabled="disabled">
                                    <option value="0">10</option>
                                    <option value="1" selected="on">20</option>
                                    <option value="2">40</option>
                                    <option value="3">60</option>
                                </select>[Sec.]</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>DGNSS Age of Correction: </td>
                            </tr>
                            <tr>
                                <td>GPS</td>
                                <td><input type="text" name="corrAgeGps" id="corrAgeGps" value="60" size="3" maxlength="3" disabled="disabled" />[Sec.]</td>
                            </tr>
                            <tr>
                                <td>GLONASS</td>
                                <td><input type="text" name="corrAgeGln" id="corrAgeGln" value="60" size="3" maxlength="3" disabled="disabled" />[Sec.]</td>
                            </tr>
                            <tr>
                                <td>Galileo</td>
                                <td><input type="text" name="corrAgeGalileo" id="corrAgeGalileo" value="60" size="3" maxlength="3" disabled="disabled" />[Sec.]</td>
                            </tr>
                            <tr>
                                <td>BeiDou</td>
                                <td><input type="text" name="corrAgeBeidou" id="corrAgeBeidou" value="60" size="3" maxlength="3" disabled="disabled" />[Sec.]</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>ITRF Realization (2014): </td>
                            </tr>
                            <tr>
                                <td>Epoch</td>
                                <td><input type="radio" name="epochVal" value="2" onclick="tplateVisibility()" disabled="disabled" />Fixed
                                            <input type="radio" name="epochVal" value="1" checked="on" onclick="tplateVisibility()" disabled="disabled" />Current</td>
                            </tr>
                            <tr id="applyItrfPosRow" style={{ display: "none" }}>
                                <td>Apply ITRF Transformation to</td>
                                <td><input type="radio" name="applyPosType" value="0" checked="on" onclick="tplateVisibility()" disabled="disabled" />None
                                            <input type="radio" name="applyPosType" value="1" onclick="tplateVisibility()" disabled="disabled" />RTX</td>
                            </tr>
                            <tr id="itrfEpochRow" style={{ display: "none" }}>
                                <td>ITRF Epoch</td>
                                <td><input type="text" name="itrfEpoch" id="itrfEpoch" value="undefined"maxlength="7" disabled="disabled" /></td>
                            </tr>
                            <tr id="tplateRow" style={{ display: "none" }}>
                                <td>Tectonic Plate</td>
                                <td id="CurTplateCell"><select name="CurTplate" id="CurTplate" disabled="disabled">
                                    <option value="0" selected="on">Auto</option>
                                    <option value="34">North America, 0.0 m</option>
                                    <option value="40">Pacific, 1160810.6 m</option>
                                    <option value="22">Juan de Fuca, 1676553.6 m</option>
                                    <option value="43">Rivera, 2020371.4 m</option>
                                    <option value="15">Cocos, 2352414.9 m</option>
                                </select></td>
                            </tr>
                            <tr id="recalcRow" style={{ display: "none" }}>
                                <td><input type="button" name="Recalc" id="Recalc" value="Recalculate" onClick="recalcAction()" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="recposition-btn">
                    <input type="button" class="mb" value="OK" onclick="Okay()" disabled="disabled" />
                    <input type="button" name="cancel" id="cancel" value="Cancel" onclick="loadDataFrameAfterDelay('xml/configDisplay.html',0)" />
                    </div>
                </div>
            </form>
        </div>
    )
}
export default ReceiverPosition