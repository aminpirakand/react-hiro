import React from "react";
import greenCircle from "../assets/img/greenCircle.png"
import "./Security.css"
const Security = () => {
    return (
        <div>
            <div className="security-main-parent">
                <p className="form-text h2 black">Security</p>
                <table className="mt security-table">
                    <tbody>
                        <tr>
                            <td align="left">Security:</td>
                            <td>Enabled with Anonymous Access</td>
                        </tr>
                        <tr>
                            <td align="left">Current User:</td>
                            <td>Not logged in.</td>
                        </tr>
                        <tr>
                            <td align="left">User Name:</td>
                            <td><input type="text" name="username" id="username" value="" size="12" maxlength="12" onKeypress="return checkForReturn(event)" /></td>
                        </tr>
                        <tr>
                            <td align="left">Password:</td>
                            <td><input type="password" name="password" id="password" value="" size="12" maxlength="12" onKeypress="return checkForReturn(event)" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><button id="submit" type="submit">Log In</button></td>
                        </tr>
                    </tbody>
                </table>
                <hr />
                <table name="users"  id="users" className="security-table" cellspacing="0" cellpadding="3">
                    <tbody>
                        <tr >
                            <th>User Name</th>
                            <th>Receiver Config</th>
                            <th>File Download</th>
                            <th>File Delete</th>
                            <th>Edit Users</th>
                            <th>NTripCaster</th>
                        </tr>
                        <tr bgcolor="white">
                            <td>Anonymous User</td>
                            <td >&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr >
                            <td>admin</td>
                            <td className="center"><img src={greenCircle}/></td>
                            <td className="center"><img src={greenCircle}/></td>
                            <td className="center"><img src={greenCircle}/></td>
                            <td className="center"><img src={greenCircle}/></td>
                            <td className="center"><img src={greenCircle}/>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}
export default Security