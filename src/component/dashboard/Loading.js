import React from "react";
import ProgressBar from 'react-bootstrap/ProgressBar';


const Loading = (props) => {
    const now = (props.number);
    const percentage = (props.percentage);
    const title = (props.title)

    // eslint-disable-next-line no-undef
    return (
        <div className="row">
            <div className="col-2 ">
                <p>{title}</p>
            </div>
            <div className="col-lg-10 col-sm-12">
                <div style={{width:"100%",height:"80px"}}>
                    <ProgressBar now={percentage} variant={props.color} label={now}   style={{height:"16px",borderRadius:"15px",border:"1px solid black",backgroundColor:'white'}} />

                </div>
            </div>

        </div>

    )


};


export default Loading