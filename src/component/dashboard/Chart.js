import React,{Component} from "react";
import {Bar} from 'react-chartjs-2';


class Chart extends Component{
    constructor(props){
        super(props);
        this.state={
            chartData:{
                labels:['A','B','C','D','E','F','G','H'],
                datasets:[
                    {
                        data:[
                            29.77,
                            0.00,
                            32.81,
                            196.46,
                            0.19,
                            98.08,
                            13.93,
                            5.14
                        ],
                        backgroundColor:[
                            "#3375a7",
                            "#9eb1ce",
                            "#db7b2a",
                            "#dba87a",
                            "#3d9441",
                            "#8ec387",
                            "#bc393e",
                            "#d18b8e",
                        ],
                    }
                ]
            }
        }
    }
    render() {
        return (

            <div className="chart chart-border" style={{width:"100%",}}>
                <Bar

                    data={this.state.chartData}

                    height={35}
                    options={{
                        legend:{
                            display:false
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    display: true ,
                                    fontColor:"black",
                                    fontSize:16

                                },
                                gridLines: {
                                    display: true,
                                    drawBorder: true,
                                    color:"white"
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    display: true ,
                                    fontColor:"black",
                                    fontSize:16

                                },
                                gridLines: {
                                    display: true,
                                    color:"white",

                                }
                            }]
                        }
                    }}

                />
            </div>

        )
    }
}
export default Chart