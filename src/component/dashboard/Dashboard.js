import React from "react";
import "./Dashboard.css"
import Chart from "./Chart";
import Loading from "./Loading";
import RadarCircle from "./Radar";
import {FaDatabase} from 'react-icons/fa';
import {MdRssFeed} from 'react-icons/md'
import {WiDirectionUp} from 'react-icons/wi'
const Dashboard = () => {

    return(
        <div>
            <div className="row first-row">
                <div className="col-lg-3 col-md-6 col-12 first-box ">
                    <div>
                        <p className="paragraph-box">Tracked Satellites</p>
                        <div className="row ">
                            <div className="col-4 font-awsome"><MdRssFeed/></div>
                            <div className="col-4" style={{fontFamily:"Nunito",fontSize:"30px"}}>22</div>
                            <div className="col-4">3 <i className="font-awsome" style={{color:"green"}} ><WiDirectionUp /></i></div>

                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-12 second-box">
                    <p className="paragraph-box">Target Server</p>
                    <div className="row ">
                        <div className="col-12 " style={{fontFamily:"Nunito",fontSize:"25px",color:"#4e73df"}}>45.252.144.14</div>
                        <div className="col-12 font-awsome" style={{textAlign:"left",margin:" 0 60px"}} ><FaDatabase/></div>
                        <div className="col-12">online</div>

                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-12 third-box">
                    <p className="paragraph-box">Target Server</p>
                    <div className="row ">
                        <div className="col-12 " style={{fontFamily:"Nunito",fontSize:"25px",color:"#4e73df"}}>45.252.144.14</div>
                        <div className="col-12 font-awsome" style={{textAlign:"left",margin:" 0 60px"}} ><FaDatabase/></div>
                        <div className="col-12">online</div>

                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-12 fourth-box">
                    <p className="paragraph-box">Target Server</p>
                    <div className="row ">
                        <div className="col-12 " style={{fontFamily:"Nunito",fontSize:"25px",color:"#4e73df"}}>45.252.144.14</div>
                        <div className="col-12 font-awsome" style={{textAlign:"left",margin:" 0 60px"}} ><FaDatabase/></div>
                        <div className="col-12">online</div>

                    </div>
                </div>

            </div>
            <div className="row chart-style">
                    <Chart/>



            </div>
            <div className="row load-and-radar">
                <div className="col-sm-12 col-lg-6 loading" >
                    <p style={{textAlign:"left"}}>Satellites Tracked / Acquired</p>
                    <Loading title="GPS" number="7/12" color="danger" percentage="60"/>
                    <Loading title="GLONASS" number="9/10" color="" percentage="90"/>
                    <Loading title="Galileo" number="2/7" color="warning" percentage="30"/>
                    <Loading title="Beidou" number="1/5" color="info" percentage="10"/>
                    <Loading title="SBAS" number="3/3" color="success" percentage="100"/>

                </div>
                <div className="col-sm-12 col-lg-6">
                    <RadarCircle/>
                </div>
            </div>
        </div>
    )
};
export default Dashboard