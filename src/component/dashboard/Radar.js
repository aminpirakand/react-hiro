import React from "react";
import Radar from 'react-d3-radar';

const RadarCircle = () => {

    return(
        <div>
            <Radar
                width={500}
                height={500}
                padding={90}
                domainMax={10}
                highlighted={null}
                onHover={(point) => {
                    if (point) {
                        console.log('hovered over a data point');
                    } else {
                        console.log('not over anything');
                    }
                }}
                data={{
                    variables: [
                        {key: 'resilience', label: '0°'},
                        {key: 'strength', label: '45°'},
                        {key: 'adaptability', label: '90°'},
                        {key: 'creativity', label: '135°'},
                        {key: 'openness', label: '180°'},
                        {key: 'confidence2', label: '225°'},
                        {key: 'confidence3', label: '270°'},
                        {key: 'confidence4', label: '318°'},


                    ],
                    sets: [
                        {
                            key: 'me',
                            label: 'My Scores',
                            values: {
                                creativity: 4,
                            },
                        },
                        {
                            key: 'me',
                            label: 'My S',
                            values: {
                                resilience: 8,
                            },
                        },
                        {
                            key: 'everyone',
                            label: 'Everyone',
                            values: {

                                adaptability: 6,

                            },
                        },
                    ],
                }}
            />
        </div>
    )
};
export default RadarCircle