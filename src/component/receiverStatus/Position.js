import React from "react";
import "./Position.css"
import { Row, Col } from "react-bootstrap";

const Position = () => {
    return (
        <div>
            <div className=" position-main-parent">
                <div className="row">
                    <div className="col-4" >
                        <p className="form-text h3">Position:</p>
                        <table className="position-table">
                                <tr>
                                    <td>
                                      <span>  Lat:</span>
                                    </td>
                                    <td>39° 53' 49.56096" N
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Lon:</nobr>
                                    </td>
                                    <td>105° 6' 56.34783" W
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Hgt:</nobr>
                                    </td>
                                    <td> 1670.461 [m]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Elevation (Ortho.):</nobr>
                                    </td>
                                    <td> 1686.890 [m,EGM96]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Type:</nobr>
                                    </td>
                                    <td>SBAS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Datum</nobr>
                                    </td>
                                    <td> WGS-84
                                    </td>
                                </tr>
                        </table>
                        <p className="form-text h3 w-100">Velocity:</p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>East:</nobr>
                                    </td>
                                    <td>0.01 [m/s]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>North:</nobr>
                                    </td>
                                    <td>0.00 [m/s]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Up:</nobr>
                                    </td>
                                    <td>0.03 [m/s]
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p className="form-text h3  w-100">Position Solution Detail:</p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>Position Dimension:</nobr>
                                    </td>
                                    <td>3D
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Position Engine:</nobr>
                                    </td>
                                    <td>Kalman
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Augmentation:</nobr>
                                    </td>
                                    <td>GPS+SBAS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Age of Corrections:</nobr>
                                    </td>
                                    <td>6.2 [Sec.]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>SBAS PRN:</nobr>
                                    </td>
                                    <td>138
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Height Mode:</nobr>
                                    </td>
                                    <td>Normal
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Correction Controls:</nobr>
                                    </td>
                                    <td>Off
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col-4 ">
                        <p className="form-text h3">Satellites Used:10
                            </p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>GPS(9):</nobr>
                                    </td>
                                    <td>2, 5, 12, 13, 15, 21, 25, 26, 29
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>SBAS(2):</nobr>
                                    </td>
                                    <td>131, 138
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p className="form-text h3">Satellites Tracked:29</p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>GPS (8):</nobr>
                                    </td>
                                    <td>2, 5, 13, 15, 21, 25, 26, 29
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>GLONASS (7):</nobr>
                                    </td>
                                    <td>1, 7, 8, 13, 14, 22, 23
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Galileo (7):</nobr>
                                    </td>
                                    <td>3, 7, 8, 13, 15, 21, 26
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>BeiDou (4):</nobr>
                                    </td>
                                    <td>11, 14, 21, 28
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>SBAS (2):</nobr>
                                    </td>
                                    <td>131, 138
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <p className="form-text h3">Receiver Clock:</p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>GPS Week:</nobr>
                                    </td>
                                    <td>2078
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>GPS Seconds:</nobr>
                                    </td>
                                    <td>138417
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Offset</nobr>
                                    </td>
                                    <td>-0.21204[msec]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Drift:</nobr>
                                    </td>
                                    <td>-2.38329[ppm]
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p className="form-text h3">Multi-System Clock Offsets:</p>
                        <table className="position-table mb-4">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>Master Clock System:</nobr>
                                    </td>
                                    <td>GPS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>GLONASS Offset:</nobr>
                                    </td>
                                    <td>-31.0 [ns]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Galileo Offset:</nobr>
                                    </td>
                                    <td>-7.5 [ns]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>BeiDou Offset:</nobr>
                                    </td>
                                    <td>-13.7 [ns]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>GLONASS Drift:</nobr>
                                    </td>
                                    <td> 0.012 [ns/s]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Galileo Drift:</nobr>
                                    </td>
                                    <td>-0.001 [ns/s]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>BeiDou Drift:</nobr>
                                    </td>
                                    <td> 0.003 [ns/s]
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col ">
                        <p className="form-text h3">Dilutions of Precision:</p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>PDOP:</nobr>
                                    </td>
                                    <td>2.4
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>HDOP:</nobr>
                                    </td>
                                    <td>1.0
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>VDOP:</nobr>
                                    </td>
                                    <td>2.3
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>TDOP:</nobr>
                                    </td>
                                    <td>1.4
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p className="form-text h3">Error Estimates(1σ):</p>
                        <table className="position-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <nobr>East:</nobr>
                                    </td>
                                    <td>0.211 [m]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>North:</nobr>
                                    </td>
                                    <td>0.220 [m]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Up:</nobr>
                                    </td>
                                    <td>0.252 [m]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Semi Major Axis:</nobr>
                                    </td>
                                    <td>0.226 [m]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Semi Minor Axis:</nobr>
                                    </td>
                                    <td>0.196 [m]
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nobr>Orientation:</nobr>
                                    </td>
                                    <td>12.7°
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    )
};
export default Position