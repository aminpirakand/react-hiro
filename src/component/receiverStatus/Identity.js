import React from "react";
import "./Identity.css";


const Identity = () => {
    return (
        <div className=" identity-main-parent">
            <p>identity</p>
            <div className="row">
                <div className="col-12">
                    <table className="identity-table" >
                        <tr>
                            <td>
                                Receiver Type:
                        </td>
                            <td>BD970</td>
                        </tr>

                        <tr>
                            <td>System Name:</td>
                            <td>BD970 Test Drive</td>
                        </tr>
                        <tr>
                            <td>Serial Number:</td>
                            <td>1051401040</td>
                        </tr>
                        <tr>
                            <td>MAC Address(Ethernet):</td>
                            <td>00:60:35:11:F7:FA</td>
                        </tr>
                        <tr>
                            <td>Ethernet IP:</td>
                            <td>155.63.160.21</td>
                        </tr>
                        <tr>
                            <td>DNS Address:</td>
                            <td>8.8.8.8</td>
                        </tr>
                        <tr>
                            <td>Secondary DNS Address:</td>
                            <td>0.0.0.0</td>
                        </tr>
                        <tr>
                            <td>DNS Resolved Name:</td>
                            <td>----</td>
                        </tr>
                        <tr>
                            <td>Zeroconf/mDNS address:</td>
                            <td>BD970.local</td>
                        </tr>
                        <tr>
                            <td>Firmware Version:</td>
                            <td>5.42</td>
                        </tr>
                        <tr>
                            <td>Core Engine Version:</td>
                            <td>5.42</td>
                        </tr>
                        <tr>
                            <td>Firmware Date:</td>
                            <td>2019-06-28</td>
                        </tr>
                        <tr>
                            <td>RTK Version:</td>
                            <td>ALGO_2018.05_T11 C146573</td>
                        </tr>
                        <tr>
                            <td>Monitor Version:</td>
                            <td>5.38</td>
                        </tr>
                        <tr>
                            <td>Antenna Database Version:</td>
                            <td>8.47</td>
                        </tr>
                        <tr>
                            <td>Hardware Version:</td>
                            <td>1.2</td>
                        </tr>
                        <tr>
                            <td>T0x Library Version:</td>
                            <td>8.112</td>
                        </tr>
                    </table>
                    <br />
                </div>
                <div className="col-12 identity-form">
                    <form >
                        <span>System Name :</span>
                        <input type="text" value="BD970 Test Drive" disabled />
                        <input type="button" value="Ok" />
                    </form>
                    <br/>
                </div>
               

            </div>



        </div>
    )
};

export default Identity