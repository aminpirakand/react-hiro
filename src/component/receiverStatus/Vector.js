import React from "react";
import "./Vector.css"
const Vector = () => {
    return (
        <div>
            <div className=" vector-main-parent ">
                <p>No Vector Available</p>
                <div className="col-md-6">
                <p className="color">Position:</p>
                <table className="vector-table">
                        <tr>
                            <td>
                                <nobr>Lat:</nobr>
                            </td>
                            <td>39° 53' 49.55199" N
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>
                                    Lon:
                                  </nobr>
                            </td>
                            <td>105° 6' 56.35139" W
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>Hgt:</nobr>
                            </td>
                            <td>1672.081 [m]
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>Type:</nobr>
                            </td>
                            <td>SBAS
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>Datum:</nobr>
                            </td>
                            <td>WGS-84
                              </td>
                        </tr>
                
                </table>
                <p className="color">Satellites Used:8
                      :</p>
                <table className="vector-table">

                
                        <tr>
                            <td>
                                <nobr>Lat:</nobr>
                            </td>
                            <td>39° 53' 49.55199" N
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>
                                    GPS(6):
                                  </nobr>
                            </td>
                            <td>2, 5, 6, 12, 25, 29
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>SBAS(2):</nobr>
                            </td>
                            <td>131, 138
                              </td>
                        </tr>
                
                </table>
                <p className="color"> Dilutions of Precision:
                  </p>
                <table className="vector-table">

               
                        <tr>
                            <td>
                                <nobr>PDOP:</nobr>
                            </td>
                            <td>1.6
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>
                                    HDOP:
                                  </nobr>
                            </td>
                            <td>0.9
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>VDOP:</nobr>
                            </td>
                            <td>1.3
                              </td>
                        </tr>
                        <tr>
                            <td>
                                <nobr>TDOP:</nobr>
                            </td>
                            <td>0.8
                              </td>
                        </tr>
                  
                </table>
                <br />
                </div>
               <div className="col-md-6"></div>
            </div>

        </div>
    )
};
export default Vector