import React, { useState } from "react";
import "./Activity.css"
const Activity = () => {
    const [gps, setGps] = useState(
        '7 , 6, 10, 11, 12, 20, 21, 22'
    );
    // const gpsChange= event =>setGps(event.target.value);

    const [glonass, setGlonass] = useState(
        ' 5 , 6, 10, 11, 12, 20, 21, 22'
    );
    const [galileo, setGalileo] = useState(
        '3, 13, 15, 21, 27, 30'
    );
    const [beiDou, setBeiDou] = useState(
        '11, 12, 21, 22, 23, 24, 25'
    );
    const [Sbas, setSbas] = useState(
        '131, 138'
    );
    const [Output, setOutput] = useState(
        'TCP/IP (5017) - CMR'
    );
    const [Temperature, setTemperature] = useState(
        '41.50°C'
    );
    return (
        <div>
            <div className="activity-main-parent">
                <div className="container">
                    <p>Satellites Tracked:25</p>
                    <table className="activity-table">
                        <tr>
                            <td>
                                <span>GPS (8):</span>
                            </td>
                            <td>
                                {gps}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>GLONASS (8):</span>
                            </td>
                            <td>
                                {glonass}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Galileo (6):</span>
                            </td>
                            <td>
                                {galileo}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>BeiDou (7):</span>
                            </td>
                            <td>
                                {beiDou}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>SBAS (2):</span>
                            </td>
                            <td>
                                {Sbas}
                            </td>
                        </tr>
                    </table>
                    <hr className="bg-secondary" />
                    <p>Input/Output:</p>
                    <table className="activity-table ">
                        <tr>
                            <td>Output : {Output}
                            </td>
                        </tr>
                    </table>
                    <hr className="bg-secondary" />
                    <table className="activity-table ">
                        <tr>
                            <td>
                                <span>Temperature:</span>
                            </td>
                            <td>{Temperature}</td>
                        </tr>
                        <tr>
                            <td>
                                <span>Runtime:</span>
                            </td>
                            <td>
                                {/*<span>{RunTime} </span>*/}
                            </td>
                        </tr>

                    </table>
                    <br />
                </div>
            </div>
        </div>
    )
};
export default Activity