import React from 'react';
import Logo from '../Logo/Logo';
import "./SideDrawer.css";
import {FaPodcast} from 'react-icons/fa';
import {GiFamilyTree} from  'react-icons/gi';
import {MdLoop} from 'react-icons/md';
import {FaLock} from 'react-icons/fa';
import {MdRssFeed} from 'react-icons/md';
import {FiBarChart} from 'react-icons/fi';
import {FaCogs} from 'react-icons/fa'
const SideDrawer = () => {

    return (
        <div className="SideDrawer">
            <Logo />
            <div >
                <ul style={{ padding: "0px 10px" }}>
                    <li className="side-list" style={{ backgroundColor: "black" }}>
                        <a href="/" style={{ color: "white" }}>
                            <i className="font-awsome"><FiBarChart/></i>
                             <span className="slider-head">Dashboard</span>
                        </a>
                    </li>
                    <li className="side-list">
                         <a className="dropdown-toggle " data-toggle="dropdown" href="" >
                             <i className="font-awsome"><FaPodcast/></i>
                        <span className="slider-head">Receiver Status</span> </a>
                        <ul className="back-list dropdown-menu">
                            <li className="side-in-list"><p className="dropdown-item-text">RECEIVER STATUS:</p></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Activity">Activity</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/position">Position</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/graphs">Graphs</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Vector">Vector</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Identity">Identity</a></li>
                        </ul>
                    </li>
                  
                    <li className="side-list"> <a className="dropdown-toggle " data-toggle="dropdown" href="">
                        <i className="font-awsome"><MdRssFeed /></i>
                        <span className="slider-head">Satellites</span>
                    </a>
                        <ul className="back-list dropdown-menu">
                            <li className="side-in-list"><p className="dropdown-item-text">SATELLITES:</p></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/general"> General</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Tracking">Traking</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Enable">Enable/Disable</a></li>
                        </ul>
                    </li> 
                    
                    <li className="side-list"> <a className="dropdown-toggle " data-toggle="dropdown" href="">
                        <i className="font-awsome"><FaCogs/></i>
                        <span className="slider-head">Receiver Configuration</span>
                    </a>
                        <ul className="back-list dropdown-menu">
                            <li className="side-in-list"><p className="dropdown-item-text">REC CONFING:</p></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Summar">Summar</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Antenna">Antenna</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Refrence">Refrence Sation</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/ReceiverTracking">Tracking</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Correction">Correction Controls</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/ReceiverPosition">Position</a></li>
                            <li className="side-in-list"><a className="dropdown-item" href="/Rest">Rest</a></li>
                        </ul>
                    </li>
                   
                    <li className="side-list"><a href="/Network">
                        <i className="font-awsome">
                            <GiFamilyTree />
                        </i>
                        <span className="slider-head">Network Configuration</span>
                        </a>
                        </li>
                    <li className="side-list"><a href="/InputOutput">
                        <i className="font-awsome"><MdLoop/></i>
                        <span className="slider-head">I/O Configuration</span>
                        </a></li>
                    <li className="side-list"><a href="/Security">
                        <i className="font-awsome"><FaLock/></i>
                        <span className="slider-head">Security</span>
                        </a></li>
                </ul>

            </div>
        </div>
    );
};
export default SideDrawer