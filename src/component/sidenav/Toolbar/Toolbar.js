import React from 'react';
import './Toolbar.css';
import GetCurrentDate from "./GetCurrentDate";
import {FaFilter} from 'react-icons/fa';
import {FaUserAlt} from 'react-icons/fa'


const Toolbar = (props) => (

        <div className="row Toolbar">
            <div className="col-4 border-right-head font-italic"><a href=""><span className="filter-icon"><FaFilter /></span>Filters</a></div>
            <div className="col-4 border-right-head"><a href="">Dashboard</a></div>
            <div className="col-3 time ">
                <GetCurrentDate />
            </div>
            <div className="col-1"><a href=""> <FaUserAlt/> </a></div>

        </div>


);
export default Toolbar;