import React from "react"
import Clock from 'react-live-clock';


const GetCurrentDate = () => {

    let newDate = new Date()
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();

    return (
        <div>
            {year + " , " + date + " , "}
            <Clock
                ticking={true}
                format={'MMMM , h:mm:ss A'}
                timezone={'IRAN/tehran'} />

        </div>

    )
};
export default GetCurrentDate