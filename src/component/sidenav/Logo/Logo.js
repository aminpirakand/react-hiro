import React from 'react';
import "./Logo.css"
import logo from '../../../assets/img/WhatsApp_Image.png'

const Logo = (props) =>(
<div className="Logo">
    <img src={logo} alt="myhiro"/>
</div>
);
export default Logo