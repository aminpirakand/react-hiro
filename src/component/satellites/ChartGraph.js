import React,{Component} from "react";
import {Bar} from 'react-chartjs-2';
import "./Chart.css";

class ChartGraph extends Component{
    constructor(props){
        super(props);
        this.state={
            chartData:{
                labels:['R1','G2','R3','E3','G5','E5','R9','G12','G13','E13','G15','E15','R17','R18','R19','B19','B20','G21','E21','G25','G26','E27'],
                datasets:[
                    {
                        data:[
                            44,
                            46,
                            48,
                            44,
                            49,
                            47,
                            43,
                            44,
                            41,
                            39,
                            43,
                            47,
                            42,
                            49,
                            40,
                            37,
                            46,
                            41,
                            42,
                            44,
                            41,
                            43,
                        ],
                        backgroundColor:[
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",
                            "#0000ff",


                        ],
                    }
                ]
            }
        }
    }
    render() {
        return (

            <div className="chart chart-border" style={{width:"100%",opacity:'0.9',}}>
                <Bar

                    data={this.state.chartData}

                    height={35}
                    options={{
                        legend:{
                            display:false
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    display: true ,
                                    fontColor:"black",
                                    fontSize:16

                                },
                                gridLines: {
                                    display: true,
                                    drawBorder: true,
                                    color:"white"
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    display: true ,
                                    fontColor:"black",
                                    fontSize:16

                                },
                                gridLines: {
                                    display: true,
                                    color:"white",

                                }
                            }]
                        }
                    }}

                />
            </div>

        )
    }
}
export default ChartGraph