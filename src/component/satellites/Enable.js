
import React, { useState } from "react";
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import "./Enable.css";




const Enable = () => {

    return (
       <div>
           <div className="network-main-parent">
           <Tabs defaultActiveKey="home" className="myTabs" >
               <Tab eventKey="home" title="GPS" className="tab " id="home">
                    <div className="row row-width">
                        <div className="col-md-4 col-12">
                            <table className="summary-table-satellite">
                                <tbody>
                                    <tr>
                                        <th>SV</th>
                                        <th>Enable</th>
                                        <th>Ignore Health</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td><input type="checkbox" checked="on" disabled/></td>
                                        <td><input type="checkbox" disabled/></td>
                                    </tr>
                                </tbody>
                            </table>
                            <form className="btn-form">
                                <input className="btn btn-danger" type="button" value="Enable All" disabled/>
                                <input className="btn lighten-btn"  type="button" value="Ignore Health All" disabled/>
                                <input className="btn facebook-btn"  type="button" value="Disable All" disabled/>
                                <input className="btn btn-dark left-ok-btn"  type="button" value="OK" disabled/>
                                <input className="btn btn-light left-ok-btn"  type="button" value="Cancel" />

                            </form>
                        </div>
                        <div className="col-md-4 col-12">
                            <table className="summary-table-satellite">
                                <tbody>
                                <tr>
                                    <th>SV</th>
                                    <th>Enable</th>
                                    <th>Ignore Health</th>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>18</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>19</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>20</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>21</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>22</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-12 col-md-4">
                            <table className="summary-table-satellite">
                                <tbody>
                                <tr>
                                    <th>SV</th>
                                    <th>Enable</th>
                                    <th>Ignore Health</th>
                                </tr>
                                <tr>
                                    <td>23</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>24</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>25</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>26</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>27</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>28</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>29</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>30</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>31</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>32</td>
                                    <td><input type="checkbox" checked="on" disabled/></td>
                                    <td><input type="checkbox" disabled/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
               </Tab>
               <Tab eventKey="GLONASS" title="GLONASS" className="tab">
                   <div className="row row-width">
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>1</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>2</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>3</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>4</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>5</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>6</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>7</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>8</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                           <form className="btn-form">
                               <input className="btn btn-danger" type="button" value="Enable All" disabled/>
                               <input className="btn lighten-btn"  type="button" value="Ignore Health All" disabled/>
                               <input className="btn facebook-btn"  type="button" value="Disable All" disabled/>
                               <input className="btn btn-dark left-ok-btn"  type="button" value="OK" disabled/>
                               <input className="btn btn-light left-ok-btn"  type="button" value="Cancel" />

                           </form>
                       </div>
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>9</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>10</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>11</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>12</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>13</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>14</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>15</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>16</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                       </div>
                       <div className="col-12 col-md-4">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>

                                   <tr>
                                       <td>17</td>
                                       <td><input type="checkbox" checked="on" disabled/></td>
                                       <td><input type="checkbox" disabled/></td>
                                   </tr>
                                   <tr>
                                       <td>18</td>
                                       <td><input type="checkbox" checked="on" disabled/></td>
                                       <td><input type="checkbox" disabled/></td>
                                   </tr>
                                   <tr>
                                       <td>19</td>
                                       <td><input type="checkbox" checked="on" disabled/></td>
                                       <td><input type="checkbox" disabled/></td>
                                   </tr>
                                   <tr>
                                       <td>20</td>
                                       <td><input type="checkbox" checked="on" disabled/></td>
                                       <td><input type="checkbox" disabled/></td>
                                   </tr>
                                   <tr>
                                       <td>21</td>
                                       <td><input type="checkbox" checked="on" disabled/></td>
                                       <td><input type="checkbox" disabled/></td>
                                   </tr>
                                   <tr>
                                       <td>22</td>
                                       <td><input type="checkbox" checked="on" disabled/></td>
                                       <td><input type="checkbox" disabled/></td>
                                   </tr>
                                   <td>23</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>

                               <tr>
                                   <td>24</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                       </div>
                   </div>
               </Tab>
               <Tab eventKey="dns" title="Galileo" className="tab">
                   <div className="row row-width">
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>1</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>2</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>3</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>4</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>5</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>6</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>7</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>8</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>9</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>10</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>11</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>12</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               </tbody>
                           </table>
                           <form className="btn-form">
                               <input className="btn btn-danger" type="button" value="Enable All" disabled/>
                               <input className="btn lighten-btn"  type="button" value="Ignore Health All" disabled/>
                               <input className="btn facebook-btn"  type="button" value="Disable All" disabled/>
                               <input className="btn btn-dark left-ok-btn"  type="button" value="OK" disabled/>
                               <input className="btn btn-light left-ok-btn"  type="button" value="Cancel" />

                           </form>
                       </div>
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>

                               <tr>
                                   <td>13</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>14</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>15</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>16</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>17</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>18</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>19</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>20</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>21</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>22</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>23</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>24</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               </tbody>
                           </table>
                       </div>
                       <div className="col-12 col-md-4">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>

                               <tr>
                                   <td>25</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>26</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>27</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>28</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>29</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>30</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>31</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>32</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>33</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>34</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>35</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>36</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               </tbody>
                           </table>
                       </div>
                   </div>
               </Tab>

               <Tab eventKey="routing" title="BeiDou" className="tab">
                   <div className="row row-width">
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>1</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>2</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>3</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>4</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>5</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>6</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>7</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>8</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>9</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>10</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                           <form className="btn-form">
                               <input className="btn btn-danger" type="button" value="Enable All" disabled/>
                               <input className="btn lighten-btn"  type="button" value="Ignore Health All" disabled/>
                               <input className="btn facebook-btn"  type="button" value="Disable All" disabled/>
                               <input className="btn btn-dark left-ok-btn"  type="button" value="OK" disabled/>
                               <input className="btn btn-light left-ok-btn"  type="button" value="Cancel" />

                           </form>
                       </div>
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>11</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>12</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>13</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>14</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>15</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>16</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>17</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>18</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>19</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>20</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                       </div>
                       <div className="col-12 col-md-4">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>21</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>22</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>23</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>24</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>25</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>26</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>27</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>28</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>29</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>30</td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                       </div>
                   </div>
               </Tab>

               <Tab eventKey="http" title="QZSS" className="tab">
                   <div className="row row-width">
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>193</td>
                                   <td><input type="checkbox" checked="on"   disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>194</td>
                                   <td><input type="checkbox" checked="on"   disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>195</td>
                                   <td><input type="checkbox" checked="on"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>196</td>
                                   <td><input type="checkbox"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               </tbody>
                           </table>
                           <form className="btn-form">
                               <input className="btn btn-danger" type="button" value="Enable All" disabled/>
                               <input className="btn lighten-btn"  type="button" value="Ignore Health All" disabled/>
                               <input className="btn facebook-btn"  type="button" value="Disable All" disabled/>
                               <input className="btn btn-dark left-ok-btn"  type="button" value="OK" disabled/>
                               <input className="btn btn-light left-ok-btn"  type="button" value="Cancel" />

                           </form>
                       </div>
                       <div className="col-md-4 col-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>
                               <tr>
                                   <td>197</td>
                                   <td><input type="checkbox"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>198</td>
                                   <td><input type="checkbox"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>199</td>
                                   <td><input type="checkbox" checked="on"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>200</td>
                                   <td><input type="checkbox"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               </tbody>
                           </table>
                       </div>
                       <div className="col-12 col-md-4">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Enable</th>
                                   <th>Ignore Health</th>
                               </tr>

                               <tr>
                                   <td>201</td>
                                   <td><input type="checkbox"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>202</td>
                                   <td><input type="checkbox"  disabled/></td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                                   <td></td>
                                   <td></td>
                               </tr>
                               <tr>
                                   <td>&nbsp;</td>
                                   <td></td>
                                   <td></td>
                               </tr>
                               </tbody>
                           </table>
                       </div>
                   </div>
               </Tab>

               <Tab eventKey="proxy" title="SBAS" className="tab">

                   <div className="row row-width">
                       <div className="col-lg-6 col-md-12">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Satellite</th>
                                   <th>Setting</th>
                                   <th>Use Obs</th>
                               </tr>
                               <tr>
                                   <td>120</td>
                                   <td>EGNOS - AOR-E	</td>
                                   <td>
                                           <select name="EnableSBAS120"
                                                   disabled="disabled" >
                                               <option value="0" style={{backgroundColor:"black"}}>Off</option>
                                               <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                               <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                               <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                                   Enable
                                               </option>
                                           </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>121</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1" >Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>122</td>
                                   <td>AUS/NZ - INMARSAT 4-F1	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>123</td>
                                   <td>	EGNOS - ASTRA-5B	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>124</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1" >Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>125</td>
                                   <td>SDCM - LUCH-5B	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>126</td>
                                   <td>EGNOS - EMEA	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>127</td>
                                   <td>GAGAN - GSAT 8	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>128</td>
                                   <td>	GAGAN - GSAT 10	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>129</td>
                                   <td>MSAS-1	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>130</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>131</td>
                                   <td>WAAS - EUTELSAT 117W B	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                               </tr>
                               <tr>
                                   <td>132</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>133</td>
                                   <td>WAAS - SES-15	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>134</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>135</td>
                                   <td>WAAS - GALAXY XV	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                               </tr>
                               <tr>
                                   <td>136</td>
                                   <td>EGNOS - SES-5	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>137</td>
                                   <td>MSAS-2	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>138</td>
                                   <td>WAAS - ANIK F1R</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}>Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} selected="1">Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" checked="on" disabled/></td>
                               </tr>
                               <tr>
                                   <td>139</td>
                                   <td>GAGAN - GSAT 15</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}}>Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                           <form className="btn-form">
                               <input className="btn btn-danger" type="button" value="Enable All" disabled/>
                               <input className="btn lighten-btn"  type="button" value="Ignore Health All" disabled/>
                               <input className="btn facebook-btn"  type="button" value="Disable All" disabled/>
                               <input className="btn btn-dark left-ok-btn"  type="button" value="OK" disabled/>
                               <input className="btn btn-light left-ok-btn"  type="button" value="Cancel" />

                           </form>
                       </div>
                       <div className="col-md-12 col-lg-6">
                           <table className="summary-table-satellite">
                               <tbody>
                               <tr>
                                   <th>SV</th>
                                   <th>Satellite</th>
                                   <th>Setting</th>
                                   <th>Use Obs</th>
                               </tr>
                               <tr>
                                   <td>140</td>
                                   <td>SDCM - LUCH-5A</td>
                                   <td>
                                       <select name="EnableSBAS120"
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"black"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>141</td>
                                   <td>SDCM - LUCH-5V</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1" >Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>142</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>143</td>
                                   <td>		</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>144</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1" >Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>145</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>146</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>147</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>148</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}  selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>149</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}  selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>150</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>151</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}  selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox"  disabled/></td>
                               </tr>
                               <tr>
                                   <td>152</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>153</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>154</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>155</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}  selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>156</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>157</td>
                                   <td>	</td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}}  selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>
                               <tr>
                                   <td>158</td>
                                   <td></td>
                                   <td>
                                       <select name="EnableSBAS120" onChange="updateSbasColor(this) "
                                               disabled="disabled" >
                                           <option value="0" style={{backgroundColor:"#FF7777"}} selected="1">Off</option>
                                           <option value="1" style={{backgroundColor:"#77FF77"}}>Enable</option>
                                           <option value="2" style={{backgroundColor:"#FFFF77"}}>Ignore Health</option>
                                           <option value="3" style={{backgroundColor:"#77FFFF"}} >Auto
                                               Enable
                                           </option>
                                       </select>
                                   </td>
                                   <td><input type="checkbox"  disabled/></td>
                               </tr>
                               <tr>
                                   <td></td>
                                   <td></td>
                                   <td>
                                   </td>
                                   <td><input type="checkbox" disabled/></td>
                               </tr>

                               </tbody>
                           </table>
                       </div>
                   </div>
               </Tab>
           </Tabs>

         </div>
       </div>
    )
};
export default Enable