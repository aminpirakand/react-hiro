import React, { useState } from "react";
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import "./Tracking.css"
import Radar from '../dashboard/Radar'
import Graph from "./Graph";
const Tracking = () => {

    return (
        <div>
            <div className="traking-main-parent">
                <Tabs defaultActiveKey="home" className="myTabs" >
                    <Tab eventKey="home" title="(Table)" className="tab " id="home">
                        <div className="traking-main-parent container">
                            <Tabs>
                                <Tab eventKey="home" title="GPS" className="tab " id="home">
                                    <div id="gps" className="tabContents" style={{ display: "block" }}>
                                        <div className="col-md-12">
                                            <table id="trackingTable" className="col-md-12 traking-table" cellspacing="0" cellpadding="3" width="">
                                                <tbody>
                                                    <tr className="gen">
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','prn');">SV</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','sys');">Type</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','elev');">Elev.<br />[Deg]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','azi');">Azim.<br />[Deg]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l1_cno');">L1-C/No<br />[dBHz]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}>L1</th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l2_cno');">L2-C/No<br />[dBHz]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}>L2</th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l5_cno');">L5-C/No<br />[dBHz]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}>L5</th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','iode');">IODE</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','ura');">URA<br />[m]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','type');">Type</a></th>
                                                    </tr>
                                                    <tr className="odd">
                                                        <td className="svs" style={{ fontSize: "14px" }}><a href="javascript:parent.showHelp('infoPRN.html?type=GPS&amp;sv=2','2')">2</a></td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>GPS</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>41.00</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>61.13</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>46.0</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>36.3</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>E</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>21</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>IIR</td>
                                                    </tr>
                                                    <tr className="">
                                                        <td className="svs" style={{ fontSize: "14px" }}><a href="javascript:parent.showHelp('infoPRN.html?type=GPS&amp;sv=5','5')">5</a></td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>GPS</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>72.16</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>98.58</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>50.3</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>48.2</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CM+CL</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>4</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>IIR-M</td>
                                                    </tr>
                                                    <tr className="odd">
                                                        <td className="svs" style={{ fontSize: "14px" }}><a href="javascript:parent.showHelp('infoPRN.html?type=GPS&amp;sv=12','12')">12</a></td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>GPS</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>30.16</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>185.71</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>47.8</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>42.7</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CM+CL</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>72</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>IIR-M</td>
                                                    </tr>
                                                    <tr className="">
                                                        <td className="svs" style={{ fontSize: "14px" }}><a href="javascript:parent.showHelp('infoPRN.html?type=GPS&amp;sv=21','21')">21</a></td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>GPS</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>10.81</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>265.63</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>37.2</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>22.3</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>E</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>17</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>IIR</td>
                                                    </tr>
                                                    <tr className="odd">
                                                        <td className="svs" style={{ fontSize: "14px" }}><a href="javascript:parent.showHelp('infoPRN.html?type=GPS&amp;sv=25','25')">25</a></td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>GPS</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>50.31</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>240.95</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>47.0</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>46.7</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CM+CL</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>48.0</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>I+Q</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>67</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>IIF</td>
                                                    </tr>
                                                    <tr className="">
                                                        <td className="svs" style={{ fontSize: "14px" }}><a href="javascript:parent.showHelp('infoPRN.html?type=GPS&amp;sv=29','29')">29</a></td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>GPS</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>51.70</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>316.73</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>49.5</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>47.3</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>CM+CL</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>16</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>IIR-M</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </Tab>
                                <Tab eventKey="ethernet" title="GLONASS" className="tab glonass-tab">
                                    <div id="glonass" className=" tabContents">
                                        <table id="trackingTable" className="svs traking-table" cellspacing="0" cellpadding="3" width="400px">
                                            <tbody>
                                                <tr className="gen">
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','prn');">SV</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','sys');">Type</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','elev');">Elev.<br />[Deg]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','azi');">Azim.<br />[Deg]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l1_cno');">L1-C/No<br />[dBHz]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}>L1</th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l2_cno');">L2-C/No<br />[dBHz]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}>L2</th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','iode');">IODE</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','ura');">URA<br />[m]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','type');">Type</a></th>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>22.52</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>32.63</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>46.1/44.6</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>40.4</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>57.17</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>90.73</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>49.3/49.2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>46.3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>2.5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>9</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>23.08</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>291.93</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>47.2/45.8</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>42.0</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>16</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>11.71</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>231.30</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>40.3/39.2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>34.2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>4</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>17</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>15.79</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>93.70</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>40.1/40.1</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>40.0</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>18</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>54.04</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>50.68</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>49.3/49.2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>46.6</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>19</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>GLONASS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>44.01</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>325.58</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>38.8/37.5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA/P</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>44.3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>65</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>M</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </Tab>
                                <Tab eventKey="dns" title="Galileo" className="tab ">
                                    <div id="galileo">
                                        <table id="trackingTable" className="svs traking-table" cellspacing="0" cellpadding="3" >
                                            <tbody>
                                                <tr className="gen">
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','prn');">SV</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','sys');">Type</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','elev');">Elev.<br />[Deg]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','azi');">Azim.<br />[Deg]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l1_cno');">L1-C/No<br />[dBHz]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}>L1</th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l5_cno');">L5-C/No<br />[dBHz]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}>L5</th>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>33.23</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>149.44</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>47.3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>44.0/44.4/46.5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>39.48</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>82.18</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>47.7</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>43.5/44.5/46.6</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>9</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>10.54</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>36.34</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>42.5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>40.4/39.6/43.0</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>15</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>55.24</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>311.04</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>47.8</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>48.1/49.2/51.3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>21</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>11.95</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>321.81</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>41.9</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>40.2/41.5/43.6</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>27</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>31.24</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>271.34</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>47.2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>43.6/45.3/47.0</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>30</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>Galileo</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>20.79</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>214.82</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>43.2</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CBOC</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>41.5/42.4/44.5</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>A/B/Alt</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </Tab>
                                <Tab eventKey="routing" title="BeiDou" className="tab">
                                    <div id="beidou" >
                                        <span id="trackingSpan">
                                            <table id="trackingTable" className="col-md-12 svs traking-table" cellspacing="0" cellpadding="3" >
                                                <tbody className="col-md-12">
                                                    <tr className="gen">
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','prn');">SV</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','sys');">Type</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','elev');">Elev.<br />[Deg]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','azi');">Azim.<br />[Deg]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l1_cno');">L1-C/No<br />[dBHz]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}>L1</th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','iode');">IODE</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','ura');">URA<br />[m]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','type');">Type</a></th>
                                                    </tr>
                                                    <tr className="odd">
                                                        <td className="svs" style={{ fontSize: "14px" }}>19</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>BeiDou</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>10.18</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>200.15</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>39.4</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>B1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>-</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>MEO</td>
                                                    </tr>
                                                    <tr className="">
                                                        <td className="svs" style={{ fontSize: "14px" }}>20</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>BeiDou</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>53.34</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>167.31</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>46.6</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>B1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>2.4</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>MEO</td>
                                                    </tr>
                                                    <tr className="odd">
                                                        <td className="svs" style={{ fontSize: "14px" }}>29</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>BeiDou</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>56.06</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>316.39</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>45.5</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>B1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>2.4</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>MEO</td>
                                                    </tr>
                                                    <tr className="">
                                                        <td className="svs" style={{ fontSize: "14px" }}>30</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>BeiDou</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>60.81</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>93.06</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>47.0</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>B1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>1</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>2.4</td>
                                                        <td className="svs" style={{ fontSize: "14px" }}>MEO</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </span>
                                    </div>
                                </Tab>
                                <Tab eventKey="http" title="QZSS" className="tab">

                                    <div id="qzss" className=" tabContents">
                                        <span id="trackingSpan">
                                            <table id="trackingTable" className="svs traking-table" cellspacing="0" cellpadding="3" width="486px">
                                                <tbody>
                                                    <tr className="gen">
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','prn');">SV</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','sys');">Type</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','elev');">Elev.<br />[Deg]</a></th>
                                                        <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','azi');">Azim.<br />[Deg]</a></th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </span>
                                    </div>

                                </Tab>
                                <Tab eventKey="proxy" title="SBASS" className="tab">

                                    <div id="sbas" class=" tabContents">
                                        <table id="trackingTable" className="svs traking-table" cellspacing="0" cellpadding="3" >
                                            <tbody>
                                                <tr className="gen">
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','prn');">SV</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('str','sys');">Type</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','elev');">Elev.<br />[Deg]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','azi');">Azim.<br />[Deg]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','l1_cno');">L1-C/No<br />[dBHz]</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}>L1</th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','iode');">IODE</a></th>
                                                    <th className="svs" style={{ fontSize: "10.5px" }}><a href="javascript:setSort('num','ura');">URA<br />[m]</a></th>
                                                </tr>
                                                <tr className="odd">
                                                    <td className="svs" style={{ fontSize: "14px" }}>131</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>SBAS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>42.25</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>198.19</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>43.0</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>41</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                </tr>
                                                <tr className="">
                                                    <td className="svs" style={{ fontSize: "14px" }}>138</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>SBAS</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>43.82</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>183.42</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>45.3</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>CA</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}>214</td>
                                                    <td className="svs" style={{ fontSize: "14px" }}> 2 </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </Tab>
                            </Tabs>
                        </div>
                    </Tab>
                    <Tab eventKey="ethernet" title="(Graph)" className="tab graph-tab">
                        <Graph/>
                    </Tab>
                    <Tab eventKey="dns" title="(Skyplot)" className="tab radar-tab">
                       <Radar/>

                    </Tab>
                </Tabs>

            </div>
        </div>
    )
}

export default Tracking