import React from "react";
import "./General.css"

const General = () => {

    return (
        <div className="general-tab">
            <div className="mb col-md-12">
                <table className="table-activity col-md-12" style={{width:"100%"}}>
                    <tbody>
                    <tr className="gen col-md-12">
                        <th className="gen"></th>
                        <th className="cellLeft" colSpan="2">Tracked</th>
                        <th className="cellLeft" colSpan="2">Constellation</th>
                    </tr>
                    <tr>
                        <th className="gen"></th>
                        <th className="svs">#</th>
                        <th className="cellLeft">Satellites</th>
                        <th className="svs">#</th>
                        <th className="cellLeft">Satellites</th>
                    </tr>
                    <tr>
                        <td className="cellRight">
                            <b>
                                GPS
                            </b>
                        </td>
                        <td className="cellCenter">6</td>
                        <td className="cellLeft">2, 5, 6, 12, 25, 29</td>
                        <td className="cellCenter">32</td>
                        <td className="cellLeft">
                            1, 2, 3, 5...17, 19...32
                            <br />
                                Unhealthy: 4, 18
                        </td>
                    </tr>
                    <tr></tr>
                    <tr className="even">
                        <td className="cellRight">
                            <b>GLONASS</b>
                        </td>
                        <td className="cellCenter">8</td>
                        <td className="cellLeft">1, 2, 3, 15...18, 24</td>
                        <td className="cellCenter">24</td>
                        <td className="cellLeft">
                            1, 2, 3, 5...24
                            <br />
                                Unhealthy: 4
                        </td>
                    </tr>
                    <tr></tr>
                    <tr className="odd">
                        <td className="cellRight">
                            <b>Galileo</b>
                        </td>
                        <td className="cellCenter">8</td>
                        <td className="cellLeft">1, 7, 8, 13, 21, 26, 31, 33</td>
                        <td className="cellCenter">22</td>
                        <td className="cellLeft">1...5, 7, 8, 9, 11, 12, 13, 15, 19, 21, 24...27, 30, 31, 33, 36</td>
                    </tr>
                    <tr></tr>
                    <tr className="even">
                        <td className="cellRight">
                            <b>QZSS</b>
                        </td>
                        <td className="cellCenter">0</td>
                        <td className="cellLeft">&nbsp;</td>
                        <td className="cellCenter">0</td>
                        <td className="cellLeft">
                            &nbsp;
                            <br />
                                Disabled:196, 197, 198, 200, 201, 202
                        </td>
                    </tr>
                    <tr></tr>
                    <tr className="odd">
                        <td className="cellRight">
                            <b>BeiDou</b>
                        </td>
                        <td className="cellCenter">5</td>
                        <td className="cellLeft">19, 20, 26, 29, 30</td>
                        <td className="cellCenter">29</td>
                        <td className="cellLeft">
                            1...14, 16, 19...30
                            <br/>
                                Unhealthy: 17, 18
                        </td>
                    </tr>
                    <tr></tr>
                    <tr className="even">
                        <td className="cellRight">
                            <b>SBAS</b>
                        </td>
                        <td className="cellCenter">2</td>
                        <td>
                            <nobr>138:ANIK F1R</nobr>
                            <br/>
                                <nobr>131:EUTELSAT 117W B</nobr>
                        </td>
                        <td className="">&nbsp;</td>
                        <td className="cellLeft">&nbsp;</td>
                    </tr>
                    <tr></tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
};

export default General