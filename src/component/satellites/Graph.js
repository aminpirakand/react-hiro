import React from "react";

import ChartGraph from "./ChartGraph";


const Graph = () => {

    return(
        <div >
            <div style={{margin:"10px"}}>
                    <table>
                        <tbody>
                        <tr>
                            <td><input type="checkbox" name="sys_GPS" id="sys_GPS" onClick="updateSignals()"
                                       checked="on"/>GPS (G)</td>
                            <td><input type="checkbox" name="sys_SBAS" id="sys_SBAS" onClick="updateSignals()"
                                       checked="on"/>SBAS (S)</td>
                            <td><input type="checkbox" name="sys_Glonass" id="sys_Glonass" onClick="updateSignals()"
                                       checked="on"/>GLONASS (R)</td>
                            <td><input type="checkbox" name="sys_Galileo" id="sys_Galileo" onClick="updateSignals()"
                                       checked="on"/>Galileo (E)</td>
                            <td><input type="checkbox" name="sys_Beidou" id="sys_Beidou" onClick="updateSignals()"
                                       checked="on"/>BeiDou (B)</td>
                            <td><input type="checkbox" name="sys_QZSS" id="sys_QZSS" onClick="updateSignals()"
                                       checked="on"/>QZSS (J)</td>
                        </tr>
                        </tbody>
                    </table>
            </div>
            <div>
                <table>
                    <tbody>
                    <tr>
                        <td style={{borderLeft:"solid 16px #0000ff"}}><input type="checkbox" name="signal_L1_0"
                                                                          id="signal_L1_0" onClick="updateSignals()"
                                                                          checked="on" />L1 CA/BOC</td>
                        <td style={{borderLeft:"solid 16px #00f000"}}><input type="checkbox" name="signal_L1EP_0"
                                                                          id="signal_L1EP_0" onClick="updateSignals()"/>L1
                            P</td>
                        <td style={{borderLeft:"solid 16px #00ffff"}}><input type="checkbox" name="signal_L1C_0"
                                                                          id="signal_L1C_0" onClick="updateSignals()"/>L1C
                        </td>
                        <td style={{borderLeft:"solid 16px #ff0000"}}><input type="checkbox" name="signal_L2EP_0"
                                                                          id="signal_L2EP_0" onClick="updateSignals()"/>L2
                            E</td>
                    </tr>
                    <tr>
                        <td style={{borderLeft:"solid 16px #ffa500"}} ><input type="checkbox" name="signal_L2C_0"
                                                                          id="signal_L2C_0" onClick="updateSignals()"/>L2
                            C/CA / B2</td>
                        <td style={{borderLeft:"solid 16px #aa8866"}} ><input type="checkbox" name="signal_E5Alt_0"
                                                                          id="signal_E5Alt_0" onClick="updateSignals()"/>E5-AltBOC
                        </td>
                        <td style={{borderLeft:"solid 16px #6688aa"}} ><input type="checkbox" name="signal_E5B_0"
                                                                          id="signal_E5B_0" onClick="updateSignals()"/>L3
                            / E5B</td>
                        <td style={{borderLeft:"solid 16px #ff00ff"}} ><input type="checkbox" name="signal_L5_0"
                                                                          id="signal_L5_0" onClick="updateSignals()"/>L5
                            / E5A</td>
                    </tr>
                    <tr>
                        <td style={{borderLeft:"solid 16px #f0e68c"}}><input type="checkbox" name="signal_L1SLAS_0"
                                                                          id="signal_L1SLAS_0"
                                                                          onClick="updateSignals()"/>L1S</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div>
                <ChartGraph/>
            </div>
        </div>

    )
};
export default Graph