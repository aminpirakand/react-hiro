import React, { useState } from "react";
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import "./Network.css"

const Network = () => {


    return (
        <div>
            <div className="network-main-parent">
                <Tabs defaultActiveKey="ethernet" className="myTabs" >
                    <Tab eventKey="home" title="Summary" className="tab " id="home">
                        <div className="container">
                            <table className="network-table" >
                                <tbody>
                                    <tr >
                                        <td className="home-network-background">DHCP Status:</td>
                                        <td>Off</td>
                                    </tr>
                                    <tr>
                                        <td>Ethernet IP:</td>
                                        <td>155.63.160.21</td>
                                    </tr>
                                    <tr >
                                        <td>DNS Address:</td>
                                        <td>8.8.8.8</td>
                                    </tr>
                                    <tr>
                                        <td className="gen_left">Secondary DNS Address:</td>
                                        <td className="gen_left">0.0.0.0</td>
                                    </tr>
                                    <tr className="odd">
                                        <td className="gen_left">HTTP Server Port:</td>
                                        <td className="gen_left">80</td>
                                    </tr>
                                    <tr>
                                        <td className="gen_left">NAT:</td>
                                        <td className="gen_left">Disabled</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </Tab>
                    <Tab eventKey="ethernet" title="Ethernet" className="tab">
                        <div className="tabContent container">
                            <form name="theForm">
                                <table className="width-table network-table">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" align="left" className="head-of-table">Stored settings</th>
                                        </tr>
                                        <tr>
                                            <td align="right">IP Setup:</td>
                                            <td className="td-left"><select name="DHCP" id="DHCP" onChange="changeDHCP()" disabled="disabled">
                                                <option value="on">DHCP</option>
                                                <option value="off" selected="on">Static IP</option>
                                            </select></td>
                                        </tr>
                                        <tr className="left-option">
                                            <td align="right">IP Address:</td>
                                            <td id="IP" className="td-left"><input className="width-input" type="text" name="IP_B1" id="IP_B1" value="155" style={{ width: "10%" }} maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="IP_B2" id="IP_B2" value="63" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="IP_B3" id="IP_B3" value="160" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="IP_B4" id="IP_B4" value="21" size="3" maxlength="3" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Netmask:</td>
                                            <td id="NM" className="td-left"><input className="width-input" type="text" name="NM_B1" id="NM_B1" value="255" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="NM_B2" id="NM_B2" value="255" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="NM_B3" id="NM_B3" value="255" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="NM_B4" id="NM_B4" value="240" size="3" maxlength="3" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Gateway:</td>
                                            <td id="GW" className="td-left"><input className="width-input" type="text" name="GW_B1" id="GW_B1" value="155" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="GW_B2" id="GW_B2" value="63" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="GW_B3" id="GW_B3" value="160" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="GW_B4" id="GW_B4" value="17" size="3" maxlength="3" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Hostname:</td>
                                            <td className="td-left"><input className="width-input" type="text" name="HOST" id="HOST" value="BD970" size="16" maxlength="16" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">MTU:</td>
                                            <td className="td-left"><input className="width-input" type="text" name="MTU" id="MTU" value="1500" size="4" maxlength="4" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Force DNS Address:</td>
                                            <td className="td-left"><input type="checkbox" name="forceDnsAddr" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">DNS Address:</td>
                                            <td id="DNS" className="td-left"><input className="width-input" type="text" name="DNS_B1" id="DNS_B1" value="8" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="DNS_B2" id="DNS_B2" value="8" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="DNS_B3" id="DNS_B3" value="8" size="3" maxlength="3" disabled="disabled" /> . <input className="width-input" type="text" name="DNS_B4" id="DNS_B4" value="8" size="3" maxlength="3" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">Sec DNS Addr:</td>
                                            <td id="Secondary_DNS" className="td-left"><input className="width-input" type="text" name="Secondary_DNS_B1" id="Secondary_DNS_B1" value="0" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="Secondary_DNS_B2" id="Secondary_DNS_B2" value="0" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="Secondary_DNS_B3" id="Secondary_DNS_B3" value="0" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="Secondary_DNS_B4" id="Secondary_DNS_B4" value="0" size="3" maxlength="3" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">DNS Domain:</td>
                                            <td className="td-left"><input type="text" className="width-input" name="DNSDomain" id="DNSDomain" value="trimble.comÐ_vÚè" size="32" maxlength="32" disabled="disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td align="right">DNS Proxy:</td>
                                            <td className="td-left"><input type="checkbox" class="mb btn btn-primary" name="dnsProxy" disabled="disabled" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <input type="button" className=" eter-btn btn-primary" name="ChangeEthConf" id="ChangeEthConf" value="Change Configuration" onclick="ChangeEthConfAction()" disabled="disabled" />&nbsp;
                                <input type="button" className="eter-btn btn-primary mb" name="cancel" id="cancel" value="Cancel" onclick="loadDataFrameAfterDelay('xml/internetSummary.html',0)" />
                                <table className="network-table">
                                    <tbody>
                                        <tr>
                                            <td colspan="4">
                                                <span>Hostname: Only alphanumeric and hyphen allowed. Required to start with letter and end with letter/number.</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr />
                                <input type="button" name="RenewDhcp" id="RenewDhcp" value="Renew DHCP" onClick="RenewDhcpAction()" disabled="disabled" style={{ display: "none" }} />
                            </form>
                            <table className="network-table">
                                <tbody>
                                    <tr>
                                        <th colspan="2">Current settings</th>
                                    </tr>
                                    <tr>
                                        <td align="right">IP Setup:</td>
                                        <td>Static IP</td>
                                    </tr>
                                    <tr>
                                        <td align="right">IP Address:</td>
                                        <td>155.63.160.21</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Netmask:</td>
                                        <td>255.255.255.240</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Gateway:</td>
                                        <td>155.63.160.17</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Hostname:</td>
                                        <td>BD970</td>
                                    </tr>
                                    <tr>
                                        <td align="right">MTU:</td>
                                        <td>1500</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </Tab>
                    <Tab eventKey="dns" title="DNS" className="tab">
                        <div id="DNS-S" className="tabcontent container">

                            <table className="network-table">
                                <tbody>
                                    <tr>
                                        <td align="right">DNS Address:</td>
                                        <td className="td-left">8.8.8.8</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Secondary DNS Address:</td>
                                        <td className="td-left">0.0.0.0</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Force DNS Address:</td>
                                        <td className="td-left"><input type="checkbox" name="forceDnsAddr" onClick="changeForceDnsAddr()" id="forceDnsAddr" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">DNS Address:</td>
                                        <td id="DNS" className="td-left"><input type="text" name="DNS_B1" id="DNS_B1" value="8" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="DNS_B2" id="DNS_B2" value="8" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="DNS_B3" id="DNS_B3" value="8" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="DNS_B4" id="DNS_B4" value="8" size="3" maxlength="3" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Sec DNS Addr:</td>
                                        <td id="Secondary_DNS" className="td-left"><input type="text" name="Secondary_DNS_B1" id="Secondary_DNS_B1" value="0" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="Secondary_DNS_B2" id="Secondary_DNS_B2" value="0" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="Secondary_DNS_B3" id="Secondary_DNS_B3" value="0" size="3" maxlength="3" disabled="disabled" /> . <input type="text" name="Secondary_DNS_B4" id="Secondary_DNS_B4" value="0" size="3" maxlength="3" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">DNS Domain:</td>
                                        <td className="td-left"><input type="text" name="DNSDomain" id="DNSDomain" value="trimble.comÐ_vÚè" size="32" maxlength="32" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">DNS Proxy:</td>
                                        <td className="td-left"><input type="checkbox" name="dnsProxy" disabled="disabled" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" className="dns-btn btn-light" name="action" id="action" value="0" disabled="disabled" />
                            <input type="button" className="dns-btn btn-light" name="ChangeDnsConf" id="ChangeDnsConf" value="Change Configuration" onclick="ChangeDnsConfAction()" disabled="disabled" />&nbsp;
                            <input type="button" className="dns-btn btn-light" name="cancel" id="cancel" value="Cancel" onclick="loadDataFrameAfterDelay('xml/internetSummary.html',0)" />

                        </div>

                    </Tab>
                    <Tab eventKey="routing" title="Routing" className="tab">
                        <div className=" tabContent container">

                            <table className="network-table" cellspacing="0" cellpadding="3">
                                <tbody>
                                    <tr>
                                        <th>Destination</th>
                                        <th>Gateway</th>
                                        <th>Mask</th>
                                        <th>Flags</th>
                                        <th>Interface</th>
                                    </tr>
                                    <tr>
                                        <td>0.0.0.0</td>
                                        <td>155.63.160.17</td>
                                        <td>0.0.0.0</td>
                                        <td>UG</td>
                                        <td>eth0 (Ethernet)</td>
                                    </tr>
                                    <tr>
                                        <td>155.63.160.16</td>
                                        <td>155.63.160.17</td>
                                        <td>255.255.255.240</td>
                                        <td>U</td>
                                        <td>eth0 (Ethernet)</td>
                                    </tr>
                                </tbody>
                            </table><br />
                            <div className="routing-btn">
                            Change default route:<select name="DefaultIntf" id="DefaultIntf" disabled="disabled">
                                <option value="20" selected="on">eth0</option>
                            </select>&nbsp;
                            <input type="button" className="rout-btn btn-danger" style={{ maxWidth: "max-content" }} name="OK1" id="OK1" value="OK" onClick="changeDefaultRoute()" disabled="disabled" />
                            </div>
                            <hr />
                            <table className="network-table">
                                <tbody>
                                    <tr>
                                        <th>Add a route</th>
                                    </tr>
                                    <tr>
                                        <td align="right">Destination:</td>
                                        <td className="td-left"><input type="text" name="dest_B1" id="dest_B1" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="dest_B2" id="dest_B2" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="dest_B3" id="dest_B3" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="dest_B4" id="dest_B4" value="" size="3" maxlength="3" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Gateway:</td>
                                        <td className="td-left"><input type="text" name="gw_B1" id="gw_B1" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="gw_B2" id="gw_B2" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="gw_B3" id="gw_B3" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="gw_B4" id="gw_B4" value="" size="3" maxlength="3" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Mask:</td>
                                        <td className="td-left"><input type="text" name="mask_B1" id="mask_B1" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="mask_B2" id="mask_B2" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="mask_B3" id="mask_B3" value="" size="3" maxlength="3" disabled="disabled" />. <input type="text" name="mask_B4" id="mask_B4" value="" size="3" maxlength="3" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Interface:</td>
                                        <td className="td-left"><select name="staticRouteIntf" id="staticRouteIntf" disabled="disabled">
                                            <option value="0" selected="on">eth0</option>
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td><input type="button" className="rout-btn btn-success pb1" style={{ maxWidth: "max-content" }} value="OK" onClick="addRoute()" disabled="disabled" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />Network Address Translation : <select name="NATEnable" id="NATEnable" disabled="disabled">
                                <option value="0" selected="on">Disable</option>
                                <option value="1">Enable</option>
                            </select>&nbsp;<input type="button" className="rout-btn btn-danger" style={{ maxWidth: "max-content" }} value="OK" onClick="changeNat()" disabled="disabled" />

                        </div>

                    </Tab>
                    <Tab eventKey="http" title="HTTP" className="tab">

                        <div id="HTTP" className=" tabContent container">

                            <table className="network-table">
                                <tbody>
                                    <tr>
                                        <td align="right">HTTP Server Port:</td>
                                        <td><input type="text" name="port" id="port" value="80" size="5" maxlength="5" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="button" class="btn btn-dark" name="OK" id="OK" value="OK" onclick="okAction()" disabled="disabled" />&nbsp;<input type="button" className="btn btn-dark" name="cancel" id="cancel" value="Cancel" onclick="loadDataFrameAfterDelay('xml/internetSummary.html',0)" /></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </Tab>
                    <Tab eventKey="proxy" title="Proxy" className="tab">

                        <div className="tabContent container">
                            <div id="contentDiv" className="proxy-network container ">Affected protocols:<ul>
                                <li className="black">FW Upgrade Check</li>
                            </ul>
                            
                            <input type="button" className="mb" value="OK" onClick="OkayAction()" disabled="disabled" />&nbsp;<input type="button" className="mb" name="cancel" id="cancel" value="Cancel" onClick="loadDataFrameAfterDelay('xml/internetSummary.html',0)" />
                            </div>
                            <table className="network-table">
                                <tbody>
                                    <tr>
                                        <td className="black">Enable HTTP proxy:</td>
                                        <td><input type="checkbox" name="enable" onClick="updateVisibility()" disabled="disabled" /></td>
                                    </tr>
                                    <tr id="httpProxy" style={{ display: "none" }}>
                                        <td>HTTP proxy:</td>
                                        <td><input type="text" name="address" id="address" value="" size="20" maxlength="59" disabled="disabled" /></td>
                                    </tr>
                                    <tr id="httpProxyPort" style={{ display: "none" }}>
                                        <td>HTTP proxy port:</td>
                                        <td><input type="text" name="port" id="port" value="0" size="20" maxlength="5" disabled="disabled" /></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </Tab>
                </Tabs>

            </div>
        </div>
    )
}
export default Network