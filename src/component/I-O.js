import React from "react"
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import "./I-o.css"
const InputOutput = () => {
    return (
        <div>
            <div className="inout-main-parent">
                <Tabs defaultActiveKey="portsummary" className="myTabs" >
                    <Tab eventKey="portsummary" title="PortSummary" className="tab" id="home">
                        <div id="tableDiv">
                            <table id="ioPortTable" className="inout-table" width="100%" cellpadding="3" cellspacing="0">
                                <tbody>
                                    <tr className="gen">
                                        <th>Type</th>
                                        <th>Port</th>
                                        <th>Input</th>
                                        <th>Output</th>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('20') ;">TCP/IP</a></td>
                                        <td>5017</td>
                                        <td>-</td>
                                        <td>CMR</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('21') ;">TCP/IP</a></td>
                                        <td>5018</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('23') ;">TCP/IP</a></td>
                                        <td>28002</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('30') ;">NTRIP Client 1</a></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('37') ;">NTRIP Client 2</a></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('38') ;">NTRIP Client 3</a></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('31') ;">NTRIP Server</a></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('32') ;">NTRIP Caster 1</a></td>
                                        <td>2101</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('33') ;">NTRIP Caster 2</a></td>
                                        <td>2102</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('34') ;">NTRIP Caster 3</a></td>
                                        <td>2103</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:gotoPort('0') ;">Serial</a></td>
                                        <td>COM1 (38.4K-8N1)</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="odd">
                                        <td><a href="javascript:gotoPort('1') ;">Serial</a></td>
                                        <td>COM2 (38.4K-8N1)</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:gotoPort('2') ;">Serial</a></td>
                                        <td>COM3 (38.4K-8N1)</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr className="disconnected">
                                        <td><a href="javascript:gotoPort('15') ;">USB</a></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </Tab>
                    <Tab eventKey="portconfiguration" title="PortConfiguration" className="tab" id="home">

                        <div id="overallDiv">
                            <form name="CommandSetup" className="inout-form">
                                <div id="portSelect" name="portSelect" className="inout-form">
                                    <span id="portMenuSpan"><select name="PortNum" id="PortNum" onchange="updateForm(true)">
                                        <option value="20">TCP/IP 5017</option>
                                        <option value="21">TCP/IP 5018</option>
                                        <option value="23">TCP/IP 28002</option>
                                        <option value="29">Add TCP/IP or UDP port</option>
                                        <option value="30">NTRIP Client 1</option>
                                        <option value="37">NTRIP Client 2</option>
                                        <option value="38">NTRIP Client 3</option>
                                        <option value="31">NTRIP Server</option>
                                        <option value="32">NTripCaster 1</option>
                                        <option value="33">NTripCaster 2</option>
                                        <option value="34">NTripCaster 3</option>
                                        <option value="0">Serial1 / COM1</option>
                                        <option value="1">Serial2 / COM2</option>
                                        <option value="2">Serial3 / COM3</option>
                                        <option value="15">USB 15</option>
                                    </select></span> <span id="streamTypeMenuSpan"><select name="StreamType" id="StreamType" onchange="updateForm(false)">
                                        <option value="0" selected="on">CMR</option>
                                        <option value="1">RTCM</option>
                                        <option value="3">NMEA</option>
                                        <option value="4">RT17/RT27</option>
                                        <option value="5">BINEX</option>
                                        <option value="6">GSOF</option>
                                        <option value="8">1 PPS TIME TAG</option>
                                        <option value="9">MET-TILT</option>
                                    </select></span>

                                </div>
                            </form>

                            <form name="portSetup" className="inout-form">
                                <div id="portSetupDiv"><input type="hidden" name="port" id="port" value="20" disabled="disabled" />
                                    <h5><span id="serverOrClientDiv">Server</span>: <span id="tcpOrUdpDiv">TCP</span> <span id="serverPortSpan"> Port: <input type="text" name="LocalPort" id="LocalPort" value="5017" style={{ width: "100px" }} maxlength="5" disabled="disabled" /></span>&nbsp;&nbsp;&nbsp;<input type="button" value="Delete" onClick="doIoSubmit( 'Delete' )" disabled="disabled" /></h5><input type="hidden" name="type" id="type" value="TCP/IP" disabled="disabled" />
                                    <table className="inout-table">
                                        <tbody>
                                            <tr id="ClientRow">
                                                <td><input type="checkbox" name="Client" onclick="updateTcpUdpSetup()" disabled="disabled" /></td>
                                                <td>Client</td>
                                            </tr><input type="hidden" name="OutputOnly" id="OutputOnly" value="on" disabled="disabled" />
                                            <tr id="OutputOnlyCbRow">
                                                <td><input type="checkbox" name="OutputOnlyCb" onclick="updateTcpUdpSetup()" checked="on" disabled="disabled" /></td>
                                                <td id="outputOnlyLabel">Output only/Allow multiple connections</td>
                                            </tr>
                                            <tr id="TcpNoDelayRow">
                                                <td><input type="checkbox" name="TcpNoDelay" onclick="updateTcpUdpSetup()" disabled="disabled" /></td>
                                                <td>Disable Nagle Algorithm</td>
                                            </tr>
                                            <tr id="OutputOnlyRadioRow" style={{ display: "none" }}>
                                                <td><input type="radio" name="OutputOnlyRadio" value="off" onclick="updateTcpUdpSetup()" disabled="disabled" /></td>
                                                <td>Input only<input type="radio" name="OutputOnlyRadio" value="on" onclick="updateTcpUdpSetup()" checked="on" disabled="disabled" />Output only</td>
                                            </tr>
                                            <tr>
                                                <td><input type="checkbox" name="UDP" onclick="updateTcpUdpSetup()" disabled="disabled" /></td>
                                                <td>UDP Mode</td>
                                            </tr>
                                            <tr id="udpTimeoutRow" style={{ display: "none" }}>
                                                <td></td>
                                                <td>UDP timeout (seconds): <input type="text" name="timeout" id="timeout" value="60" size="2" maxlength="2" disabled="disabled" /></td>
                                            </tr>
                                            <tr id="udpBroadcastTxRow" style={{ display: "none" }}>
                                                <td></td>
                                                <td><input type="checkbox" name="udpBroadcastTxCb" onclick="updateTcpUdpSetup()" disabled="disabled" />UDP Broadcast Transmit<span id="udpTxBroadcastPortSpan" style={{ display: "none" }}>: <input type="text" name="udpTxBroadcastPort" id="udpTxBroadcastPort" value="5017" size="5" maxlength="5" disabled="disabled" /></span></td>
                                            </tr>
                                            <tr id="udpBroadcastRxRow" style={{ display: "none" }}>
                                                <td></td>
                                                <td><input type="checkbox" name="udpBroadcastRxCb" onclick="updateTcpUdpSetup()" disabled="disabled" />UDP Broadcast Receive</td>
                                            </tr>
                                            <tr id="authenticateRow">
                                                <td><input type="checkbox" name="authenticate" onclick="updateTcpUdpSetup()" disabled="disabled" /></td>
                                                <td>Authenticate, set password:</td>
                                            </tr><input type="hidden" name="fakePassword" id="fakePassword" value="" disabled="disabled" />
                                            <tr id="pwRow" style={{ display: "none" }}>
                                                <td></td>
                                                <td>
                                                    <table className="inout-table" >
                                                        <tbody>
                                                            <tr>
                                                                <td>Password:</td>
                                                                <td><input type="password" name="auth_pass" id="password" value="" size="20" maxlength="7" disabled="disabled" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Verify Password:</td>
                                                                <td><input type="password" name="auth_pass2" id="password2" value="" size="20" maxlength="7" disabled="disabled" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="remoteIpDiv" style={{ display: "none" }} >Remote IP: <input type="text" name="RemoteAddr" id="RemoteAddr" value="" size="50" maxlength="50" disabled="disabled" /> : <input type="text" name="RemotePort" id="RemotePort" value="" size="5" maxlength="5" disabled="disabled" /></div>
                                </div>
                                <div id="portStatusDiv">
                                    <h5 style={{paddingLeft:"5%"}}>Input/Output</h5>
                                    <span><a className="black" href="javascript:selectStreamType('CMR')">Output:CMR</a></span>
                                </div>
                                <div id="streamSetupDiv">
                                    <h3 className="margin-left-io">CMR</h3><input type="hidden" name="datatype" id="datatype" value="cmr" disabled="disabled" />
                                    <table className="inout-table">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select name="CMR" id="CMR" onChange="updateCmrConfig()" disabled="disabled">
                                                        <option value="0">Disabled</option>
                                                        <option value="1">CMR+</option>
                                                        <option value="8">CMR+ 2 Seconds</option>
                                                        <option value="2">CMR</option>
                                                        <option value="3" selected="on">Moving Base CMR 5Hz</option>
                                                        <option value="4">Moving Base CMR 10Hz</option>
                                                        <option value="5">Moving Base CMR 20Hz</option>
                                                        <option value="6">sCMRx</option>
                                                    </select>
                                                </td>
                                                <td id="CMRDelayLabel" align="right">Delay:</td>
                                                <td>
                                                    <select name="CMRDelay" id="CMRDelay" disabled="disabled">
                                                        <option value="0" selected="on">0 msec</option>
                                                        <option value="50">50 msec</option>
                                                        <option value="100">100 msec</option>
                                                        <option value="150">150 msec</option>
                                                        <option value="200">200 msec</option>
                                                        <option value="250">250 msec</option>
                                                        <option value="300">300 msec</option>
                                                        <option value="350">350 msec</option>
                                                        <option value="400">400 msec</option>
                                                        <option value="450">450 msec</option>
                                                        <option value="500">500 msec</option>
                                                        <option value="550">550 msec</option>
                                                        <option value="600">600 msec</option>
                                                        <option value="650">650 msec</option>
                                                        <option value="700">700 msec</option>
                                                        <option value="750">750 msec</option>
                                                        <option value="800">800 msec</option>
                                                        <option value="850">850 msec</option>
                                                        <option value="900">900 msec</option>
                                                        <option value="950">950 msec</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="CmrBwLimitDiv" style={{ display: "none" }}>
                                        <input type="checkbox" name="BWValid" id="BWValid" onClick="updateCmrConfig()" disabled="disabled" />Bandwidth limit : <input type="text" name="BWLimit" id="BWLimit" value="0" maxlength="5" disabled="disabled" style={{ display: "none" }} />
                                        <font id="BWLimitUnits" style={{ display: "none" }}>[bytes/sec]</font>
                                    </div>
                                    <input type="button" className="mb margin-left-io" value="OK" onClick="doIoSubmit('OK' )" disabled="disabled" />&nbsp;&nbsp;<input type="button" name="cancel" id="cancel" value="Cancel" onClick="loadDataFrameAfterDelay('xml/ioConfigDisplay.html',0)" />
                                </div>

                            </form>
                        </div>

                    </Tab>
                </Tabs>
            </div>
        </div>
    )
}
export default InputOutput