import React from 'react';
import "./App.css"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Graphs from "./component/receiverStatus/Graphs";
import Position from "./component/receiverStatus/Position";
import Activity from "./component/receiverStatus/Activity";
import Vector from "./component/receiverStatus/Vector";
import Summar from "./component/receiverConfiguration/Summar";
import Correction from "./component/receiverConfiguration/Correction"
import Identity from "./component/receiverStatus/Identity";
import Antenna from './component/receiverConfiguration/Antenna';
import Refrence from './component/receiverConfiguration/Refrence';
import ReceiverTracking from './component/receiverConfiguration/ReciverTracking';
import ReceiverPosition from './component/receiverConfiguration/ReceiverPosition';
import Rest from './component/receiverConfiguration/Rest';
import Network from './component/Network';
import Tracking from './component/satellites/Tracking';
import InputOutput from './component/I-O'
import Security from './component/Security';
import Toolbar from './component/sidenav/Toolbar/Toolbar';
import SideDrawer from './component/sidenav/SideDrawer/SideDrawer';
import Footer from './component/Footer';
import Dashboard from "./component/dashboard/Dashboard";
import General from "./component/satellites/General"
import Enable from "./component/satellites/Enable";
function App() {
  return (
      <Router>
        <div className="App">
          <div className="row ">
            <div className="col-2 sidenav">
              <SideDrawer />
            </div>
            <div className="col-10 main " >
              <div className="row main-top">
                <div className="col-12">
                  <Toolbar />
                </div>
              </div>
              <div className="row main-center container-fluid ">
                <div className="col-12">
                  <Switch>
                    <Route exact path={'/'} component={Dashboard}/>
                    <Route path={'/graphs'} component={Graphs} />
                    <Route path={'/enable'} component={Enable} />
                    <Route path={'/position'} component={Position} />
                    <Route path={'/Activity'} component={Activity} />
                    <Route path={'/Vector'} component={Vector} />
                    <Route path={'/Summar'} component={Summar} />
                    <Route path={'/Correction'} component={Correction} />
                    <Route path={'/Identity'} component={Identity} />
                    <Route path={'/Antenna'} component={Antenna} />
                    <Route path={'/Refrence'} component={Refrence} />
                    <Route path={'/ReceiverTracking'} component={ReceiverTracking} />
                    <Route path={'/ReceiverPosition'} component={ReceiverPosition} />
                    <Route path={'/Rest'} component={Rest} />
                    <Route path={'/Network'} component={Network} />
                    <Route path={'/general'} component={General} />
                    <Route path={'/Tracking'} component={Tracking} />
                    <Route path={'/InputOutput'} component={InputOutput} />
                    <Route path={'/Security'} component={Security} />
                  </Switch>
                </div>


              </div>
              <div className="row main-bottom">
                <div className="col-12 ">
                  <Footer />
                </div>

              </div>
            </div>

          </div>
        </div>
      </Router >
  );
}

export default App;
